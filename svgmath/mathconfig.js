Storage.prototype.setObject = function(key, value) {
    this.setItem(key, JSON.stringify(value));
};

Storage.prototype.getObject = function(key) {
	var value = this.getItem(key);
	return value && JSON.parse(value);
};

var MathConfig = (function() {
	var MathConfig = function(config_path) {
		var req = new XMLHttpRequest();
		req.open('GET', config_path, false);
		req.send(null);
		var config;
		if(req.status == 200) {
			config = JSON.parse(req.responseText);
		}
		else {
			//TODO: Error
		}

		this.verbose = false;
		this.debug = [];
		this.currentFamily = null;
		this.fonts = {};
		this.variants = {};
		this.defaults = config['defaults'] || {};
		this.opstyles = {};
		this.fallbackFamilies = [];

		var i; var j;

		if(config['fallback']) {
			var ff = config['fallback'].split(',');
			for(i = 0; i < ff.length; i++) {
				var f = ff[i].trim();
				this.fallbackFamilies.push(f);
			}
		}

		if(config['families']) {
			for(i = 0; i < config['families'].length; i++) {
				var family = config['families'][i];
				for(j = 0; j < family['fonts'].length; j++) {
					var font = family['fonts'][j];
					var weight = font['weight'] || 'normal';
					var style = font['style'] || 'normal';
					var metricurl = font['metric'];

					var identifier = weight + ' ' + style + ' ' + family['name'];

					var metric;
					var request = new XMLHttpRequest();
					request.open('GET', metricurl, false);
					request.send(null);

					if(request.status == 200) {
						//console.log('loaded from web: ', identifier);
						metric = JSON.parse(request.responseText);
					}

					this.fonts[identifier] = metric;
				}
			}
		}

		if(config['operator-styles']) {
			for(i = 0; i < config['operator-styles'].length; i++) {
				var os = config['operator-styles'][i];
				this.opstyles[os['operator']] = os['styles'];
			}
		}

		if(config['mathvariants']) {
			for(i = 0; i < config['mathvariants'].length; i++) {
				var variant = config['mathvariants'][i];
				var name = variant['name'];
				var fs = (variant['family'] || '').split(',');
				var families = [];
				for(j = 0; j < fs.length; j++) {
					families.push(fs[j].trim());
				}
				var weightattr = variant['weight'] || 'normal';
				var styleattr = variant['style'] || 'normal';
				this.variants[name] = [weightattr, styleattr, families];
			}
		}
	};

	MathConfig.prototype.findfont = function(weight, style, family) {
		weight = (weight || 'normal').trim();
		style = (style || 'normal').trim();
		family = (family || '').trim();

		var metric;

		metric = this.fonts[weight + ' ' + style + ' ' + family];
		if(metric) return metric;

		metric = this.fonts[weight + ' normal ' + family];
		if(metric) return metric;

		metric = this.fonts['normal ' + style + ' ' + family];
		if(metric) return metric;

		metric = this.fonts['normal normal ' + family];
		if(metric) return metric;

		return null;
	};

	return MathConfig;
})();