from lib import ttf
import argparse
import json


def convert_char_metric(cm):
    return {
        'name': cm.name,
        'bbox': cm.bbox,
        'width': cm.width,
        'code': cm.codes
    }

parser = argparse.ArgumentParser()
parser.add_argument('input', type=str)
args = parser.parse_args()

font = ttf.TTFMetric(args.input)

chars = {}
for k, v in font.chardata.items():
    cdata = convert_char_metric(v)
    chars[k] = cdata

fontdata = {
    'chardata': chars,
    'fullname': font.fullname,
    'ascender': font.ascender,
    'axisposition': font.axisposition,
    'bbox': font.bbox,
    'capheight': font.capheight,
    'charwidth': font.charwidth,
    'descender': font.descender,
    'family': font.family,
    'fontname': font.fontname,
    'italicangle': font.italicangle,
    'missingGlyph': convert_char_metric(font.missingGlyph),
    'numGlyphs': font.numGlyphs,
    'rulewidth': font.rulewidth,
    'stdhw': font.stdhw,
    'stdvw': font.stdvw,
    'underlineposition': font.underlineposition,
    'underlinethickness': font.underlinethickness,
    'unitsPerEm': font.unitsPerEm,
    'vgap': font.vgap,
    'weight': font.weight,
    'xheight': font.xheight
}

print json.dumps(fontdata)

