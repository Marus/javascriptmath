var MathOperators = (function() {
	var MathOperators = function(operator_path) {
		var req = new XMLHttpRequest();
		req.open('GET', operator_path, false);
		req.send(null);
		if(req.status == 200) {
			this.operatorDictionary = JSON.parse(req.responseText);
		}
		else {
			//TODO: Error
		}
	};

	MathOperators.prototype.lookup = function(op, form) {
		if(form === undefined) form = 'infix';
		var res;
		res = this.operatorDictionary[op + form];
		if(res) return res;

		res = this.operatorDictionary[op + 'infix'];
		if(res) return res;

		res = this.operatorDictionary[op + 'postfix'];
		if(res) return res;

		res = this.operatorDictionary[op + 'prefix'];
		if(res) return res;

		return this.operatorDictionary['infix'];
	};

	return MathOperators;
})();