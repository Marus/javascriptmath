String.prototype.format = function() {
    var formatted = this;
    for (var i = 0; i < arguments.length; i++) {
        var regexp = new RegExp('\\{'+i+'\\}', 'gi');
        formatted = formatted.replace(regexp, arguments[i]);
    }
    return formatted;
};

var svgmath = (function() {
    var globalDefaults = {
        "mathvariant": "normal",
        "mathsize": "18pt",
        "mathcolor": "black",
        "mathbackground": "transparent",
        "displaystyle": "false",
        "scriptlevel": "0",
        "scriptsizemultiplier": "0.71",
        "scriptminsize": "8pt",
        "veryverythinmathspace": "0.0555556em",
        "verythinmathspace": "0.111111em",
        "thinmathspace": "0.166667em",
        "mediummathspace": "0.222222em",
        "thickmathspace": "0.277778em",
        "verythickmathspace": "0.333333em",
        "veryverythickmathspace": "0.388889em",
        "linethickness": "1",
        "bevelled": "false",
        "enumalign": "center",
        "denomalign": "center",
        "lquote": "\"",
        "rquote": "\"",
        "height": "0ex",
        "depth": "0ex",
        "width": "0em",
        "open": "(",
        "close": ")",
        "separators": ",",
        "notation": "longdiv",
        "align": "axis",
        "rowalign": "baseline",
        "columnalign": "center",
        "columnwidth": "auto",
        "equalrows": "false",
        "equalcolumns": "false",
        "rowspacing": "1.0ex",
        "columnspacing": "0.8em",
        "framespacing": "0.4em 0.5ex",
        "rowlines": "none",
        "columnlines": "none",
        "frame": "none"
    };

    var specialChars = {
        '\u2145': 'D',
        '\u2146': 'd',
        '\u2147': 'e',
        '\u2148': 'i',
        '\u00A0': ' '
    };

    var alignKeywords = { "left": 0, "center": 0.5, "right": 1 };

    var normalize_string = function(str) {
        return str.replace(/ +/g, ' ').trim();
    };

    var defaultSlope = 1.383;

    var getByIndexOrLast = function(lst, idx) {
        if(idx < lst.length) return lst[idx];
        else return lst[lst.length - 1];
    };

    var createCellDescriptor = function(content, halign, valign, colspan, rowspan) {
        return {
            'content': content,
            'halign': halign,
            'valign': valign,
            'colspan': colspan,
            'rowspan': rowspan
        };
    };

    var createColumnDescriptor = function() {
        return {
            'auto': true,
            'fit': false,
            'width': 0,
            'spaceAfter': 0,
            'lineAfter': null
        };
    };

    var createRowDescriptor = function(node, cells, rowalign, columnaligns, busycells) {
        var descriptor = {
            'alignToAxis': (rowalign == 'axis'),
            'height': 0,
            'depth': 0,
            'spaceAfter': 0,
            'lineAfter': null,
            'cells': []
        };

        for(var i = 0; i < cells.length; i++) {
            var c = cells[i];
            while(busycells.length > descriptor.cells.length && busycells[descriptor.cells.length] > 0) {
                descriptor.cells.push(null);
            }

            var halign = getByIndexOrLast(columnaligns, descriptor.cells.length);
            var valign = rowalign;
            var colspan = 1;
            var rowspan = 1;

            if(c.element.tagName == 'mtd') {
                halign = c.getAttribute('columnalign', halign);
                valign = c.getAttribute('rowalign', valign);
                var cspan = c.getAttribute('colspan', '1');
                colspan = c.getAttribute('columnspan', cspan);
                rowspan = c.getAttribute('rowspan', rowspan);
            }

            while(descriptor.cells.length >= node.columns.length) {
                node.columns.push(createColumnDescriptor());
            }

            descriptor.cells.push(createCellDescriptor(c, halign, valign, colspan, rowspan));

            for(var j = 1; j < colspan; j++) {
                descriptor.cells.push(null);
            }

            while(descriptor.cells.length > node.columns.length) {
                node.columns.push(createColumnDescriptor());
            }
        }

        return descriptor;
    };

    var SVG_NS = "http://www.w3.org/2000/svg";
    var SVG_MATH_NS = "http://www.grigoriev.ru/svgmath";

    var createDocument = function(ns, re, other) {
        var doc = document.implementation.createDocument(ns, re, other);
        return doc;
    };

    var createSVGNode = function(doc, tagName, attrs) {
        var e = doc.createElementNS(SVG_NS, tagName);
        for(var attrname in attrs) {
            e.setAttribute(attrname, attrs[attrname]);
        }
        return e;
    };

    var createSVGMATHNode = function(doc, tagName, attrs) {
        var e = doc.createElementNS(SVG_MATH_NS, tagName);
        for(var attrname in attrs) {
            e.setAttribute(attrname, attrs[attrname]);
        }
        return e;
    };

    var MathNode = function(element, parent, config) {
        this.element = element;
        this.parent = parent;
        this.config = config;
        this.children = [];
        
        if(parent) {
            this.nodeIndex = parent.children.length;
            this.defaults = parent.defaults;
            parent.children.push(this);
        }
        else {
            this.defaults = JSON.parse(JSON.stringify(globalDefaults));
            this.nodeIndex = 0;
        }

        var i;

        if (['mi', 'mn', 'mo', 'ms', 'mtext'].indexOf(this.element.tagName) >= 0) {
            this.text = this.element.textContent;
            if(this.text.length > 1) {
                for(i = 0; i < this.text.length; i++) {
                    var chcode = this.text.charCodeAt(i);
                    if(chcode >= 0x0300 && chcode <= 0x036F) {
                        var pretext = this.text.substring(0, i);
                        var combiner = this.text.substring(i, i+1);
                        var posttext = this.text.substring(i+1);
                        console.log('Found splitting: ', 'pre: ', pretext, 'combiner: ', combiner, 'post: ', posttext);
                        var combinernode;
                        if(pretext === '') {
                            this.text = combiner;
                            combinernode = this;
                            this.isCombiner = true;
                        }
                        else {
                            this.text  = pretext;
                            var cattrs = {
                                'lspace': '0px',
                                'rspace': this.getProperty('rspace')
                            };
                            this.element.setAttribute('rspace', '0px');

                            combinernode = this.createSibling(this.element.tagName, cattrs, combiner);
                            combinernode.isCombiner = true;
                        }

                        if(posttext !== '') {
                            var pattrs = {
                                'lspace': '0px',
                                'rspace': combinernode.getProperty('rspace')
                            };
                            combinernode.element.setAttribute('rspace', '0px');

                            this.createSibling(this.element.tagName, pattrs, posttext);
                        }

                        break;
                    }
                }
            }
        }
        else {
            for(i = 0; i < this.element.childNodes.length; i++) {
                new MathNode(this.element.childNodes[i], this, this.config);
            }
        }
    };

    MathNode.prototype.createNode = function(tagName, attributes) {
        var e = this.element.ownerDocument.createElement(tagName);
        for(var attribute in attributes) {
            e.setAttribute(attribute, attributes[attribute]);
        }
        return new MathNode(e, this, this.config);
    };

    MathNode.prototype.createSibling = function(tagName, attributes, text) {
        var e = this.element.ownerDocument.createElement(tagName);
        for(var attribute in attributes) {
            e.setAttribute(attribute, attributes[attribute]);
        }
        e.appendChild(this.element.ownerDocument.createTextNode(text));
        return new MathNode(e, this.parent, this.config);
    };

    MathNode.prototype.makeContext = function() {
        var method = this['context_' + this.element.tagName];
        if(method) method.call(this);
        else this.default_context();
    };

    MathNode.prototype.makeChildContext = function(child) {
        var method = this['child_context_' + this.element.tagName];
        if(method) method.call(this, child);
        else this.default_child_context(child);
    };

    MathNode.prototype.measure = function() {
        this.makeContext();
        for(var i = 0; i < this.children.length; i++) {
            this.children[i].measure();
        }
        this.measureNode();
    };

    MathNode.prototype.measureNode = function() {
        var method = this['measure_' + this.element.tagName];
        if(method) method.call(this);
        else this.default_measure();
    };

    MathNode.prototype.warning = function(msg) {
        if(console && console.log) console.log('WARNING: ' + msg);
    };

    MathNode.prototype.error = function(msg) {
        if(console && console.log) console.log('ERROR: ' + msg);
        this.setAttribute('background-color', 'red');
    };

    MathNode.prototype.info = function(msg) {
        if(console && console.log) console.log('INFO: ' + msg);
    };

    MathNode.prototype.debug = function(msg) {
        if(console && console.log) console.log('DEBUG: ' + msg);
    };

    MathNode.prototype.parseInt = function(x) {
        var val = parseInt(x, 10);
        if(isNaN(val)) {
            this.error('Cannot parse ' + x + ' as an integer');
            return 0;
        }
        return val;
    };

    MathNode.prototype.parseFloat = function(x) {
        var val = parseFloat(x);
        if(isNaN(val)) {
            this.error('Cannot parse ' + x + ' as a float');
            return 0;
        }
        return val;
    };

    MathNode.prototype.parseLength = function(lenattr, unitlessScale) {
        if(unitlessScale === undefined) unitlessScale = 0.75;
        lenattr = lenattr.trim();
        var unit = lenattr.substr(lenattr.length - 2);
        var num = lenattr.substr(0, lenattr.length - 2);
        if(unit == 'pt') return this.parseFloat(num);
        else if(unit == 'mm') return this.parseFloat(num) * (72.0/25.4);
        else if(unit == 'cm') return this.parseFloat(num) * (72.0/2.54);
        else if(unit == 'in') return this.parseFloat(num) * 72.0;
        else if(unit == 'pc') return this.parseFloat(num) * 12.0;
        else if(unit == 'px') return this.parseFloat(num) * (72.0/96.0);
        else if(unit == 'em') return this.parseFloat(num) * this.fontSize;
        else if(unit == 'ex') return this.parseFloat(num) * this.fontSize * this.metric().xheight;
        else return this.parseFloat(lenattr) * unitlessScale;
    };

    MathNode.prototype.parseSpace = function(spaceattr, unitlessScale) {
        var sign = 1.0;
        spaceattr = spaceattr.trim();
        if(spaceattr.match(/mathspace$/)) {
            if(spaceattr.match(/^negative/)) {
                sign = -1.0;
                spaceattr = spaceattr.substr(8);
            }
            var realspaceattr = this.defaults[spaceattr];
            if(realspaceattr === null) {
                this.error('Bad space token: ' + spaceattr);
                realspaceattr = '0em';
            }
            return this.parseLength(realspaceattr, unitlessScale);
        }
        return this.parseLength(spaceattr, unitlessScale);
    };

    MathNode.prototype.parsePercent = function(lenattr, percentBase) {
        var value = this.parseFloat(lenattr.substr(0, lenattr.length - 1));
        if(value !== null) return percentBase * (value / 100.0);
        else return 0;
    };

    MathNode.prototype.parseLengthOrPercent = function(lenattr, percentBase, unitlessScale) {
        lenattr = lenattr.trim();
        if(lenattr.match(/%$/)) return this.parsePercent(lenattr, percentBase);
        else return this.parseLength(lenattr, unitlessScale);
    };

    MathNode.prototype.parseSpaceOrPercent = function(lenattr, percentBase, unitlessScale) {
        lenattr = lenattr.trim();
        if(lenattr.match(/%$/)) return this.parsePercent(lenattr, percentBase);
        else return this.parseSpace(lenattr, unitlessScale);
    };

    MathNode.prototype.hasAttribute = function(key) {
        return this.getAttribute(key) !== null;
    };

    MathNode.prototype.getAttribute = function(key, value) {
        if(value === undefined) value = null;
        var attr = this.element.attributes[key];
        if(attr === undefined) return value;
        else return attr.value;
    };

    MathNode.prototype.getProperty = function(key, defaultValue) {
        var attr = this.element.attributes[key];
        if(attr === undefined) {
            var value = this.defaults[key];
            if (value === undefined) return defaultValue;
            else return value;
        }
        else return attr.value;
    };

    MathNode.prototype.getListProperty = function(key, value) {
        if(value === null || value === undefined) value = this.getProperty(key);
        var splitvalue = value.split(' ');
        if(splitvalue.length > 0) return splitvalue;
        this.error('Bad value for ' + key + ' attribute: empty list');
        return this.defaults[key].split(' ');
    };
    
    MathNode.prototype.fontpool = function() {
        if(this.metriclist === undefined || this.metriclist === null) {
            var fillMetricList = function(familyList, weight, style) {
                var metriclist = [];
                for(var i = 0; i < familyList.length; i++) {
                    var family = familyList[i];
                    var metric = this.config.findfont(weight, style, family);
                    if(metric !== null) {
                        metriclist.push({ family: family, metric: metric, used: false });
                    }
                }
                if(metriclist.length === 0) {
                    this.warning('Cannot find any font metric for family: ' + familyList + ' ' + weight + ' ' + style);
                    return null;
                }
                return metriclist;
            };
            this.metriclist = fillMetricList.call(this, this.fontfamilies, this.fontweight, this.fontstyle);
            if(this.metriclist === null) {
                this.fontfamilies = this.config.fallbackFamilies;
                this.metriclist = fillMetricList.call(this, this.fontfamilies, this.fontweight, this.fontstyle);
            }
            if(this.metriclist === null) {
                throw "Fatal Error: Cannot find font metric for the node.";
            }
        }
        return this.metriclist;
    };

    MathNode.prototype.metric = function() {
        if(this.nominalMetric === undefined || this.nominalMetric === null) {
            this.nominalMetric = this.fontpool()[0].metric;
            for(var i = 0; i < this.metriclist.length; i++) {
                var fd = this.metriclist[i];
                if(fd.used) {
                    this.nominalMetric = fd.metric;
                    break;
                }
            }
        }
        return this.nominalMetric;
    };

    MathNode.prototype.axis = function() {
        return this.metric().axisposition * this.fontSize;
    };

    MathNode.prototype.nominalLineWidth = function() {
        return this.metric().rulewidth * this.fontSize;
    };

    MathNode.prototype.nominalThinStrokeWidth = function() {
        return 0.04 * this.originalFontSize;
    };

    MathNode.prototype.nominalMediumStrokeWidth = function() {
        return 0.06 * this.originalFontSize;
    };

    MathNode.prototype.nominalThickStrokeWidth = function() {
        return 0.08 * this.originalFontSize;
    };

    MathNode.prototype.nominalLineGap = function() {
        return this.metric().vgap * this.fontSize;
    };

    MathNode.prototype.nominalAscender = function() {
        return this.metric().ascender * this.fontSize;
    };

    MathNode.prototype.nominalDescender = function() {
        return -this.metric().descender * this.fontSize;
    };

    MathNode.prototype.hasGlyph = function(ch) {
        var fp = this.fontpool();
        for(var i = 0; i < fp.length; i++) {
            var font = fp[i];
            if(font.metric.chardata[ch] !== undefined) {
                return true;
            }
        }
        return false;
    };

    MathNode.prototype.findChar = function(ch) {
        var fp = this.fontpool();
        var i;
        for(i = 0; i < fp.length; i++) {
            var font = fp[i];
            var cm = font.metric.chardata[ch];
            if(cm) {
                return [cm, font];
            }
        }

        var mapping = specialChars[String.fromCharCode(ch)];
        if(mapping) {
            return this.findChar(mapping.charCodeAt(0));
        }

        // If we can't find a character definition then add in the fallback font families
        for(i = 0; i < this.config.fallbackFamilies.length; i++) {
            var ff = this.config.fallbackFamilies[i];
            if(this.fontfamilies.indexOf(ff) <= 0) {
                this.fontfamilies.push(ff);
                this.metriclist = null;
                return this.findChar(ch);
            }
        }

        return null;
    };

    MathNode.prototype.getUCSText = function() {
        var codes = [];
        for(var i = 0; i < this.text.length; i++) {
            codes.push(this.text.charCodeAt(i));
        }
        return codes;
    };

    MathNode.prototype.measureText = function() {
        var ucstext = this.getUCSText();
        if(ucstext.length === 0) {
            this.isSpace = true;
            return 0;
        }

        var cm0; var cm1;

        for(var i = 0; i < ucstext.length; i++) {
            var chcode = ucstext[i];
            var chardesc = this.findChar(chcode);
            if(chardesc === null) {
                this.width += this.metric().missingGlyph.width;
            }
            else {
                var cm = chardesc[0];
                var fd = chardesc[1];
                fd.used = true;
                if (chcode == ucstext[0]) cm0 = cm;
                if (chcode == ucstext[ucstext.length-1]) cm1 = cm;
                this.width += cm.width;
                if(this.height + this.depth === 0) {
                    this.height = cm.bbox[3];
                    this.depth = -cm.bbox[1];
                }
                else if(cm.bbox[3] != cm.bbox[1]) {
                    this.height = Math.max(this.height, cm.bbox[3]);
                    this.depth = Math.max(this.depth, -cm.bbox[1]);
                }
            }
        }

        this.height *= this.fontSize;
        this.depth *= this.fontSize;

        this.width *= this.fontSize;
        this.ascender = this.nominalAscender();
        this.descender = this.nominalDescender();

        if(cm0 !== undefined) this.leftBearing = Math.max(0, -cm0.bbox[0]) * this.fontSize;
        if(cm1 !== undefined) this.rightBearing = Math.max(0, cm1.bbox[2] - cm1.width) * this.fontSize;
        this.width += this.leftBearing + this.rightBearing;

        this.nominalMetric = null;
    };

    MathNode.prototype.default_context = function() {
        if(this.parent) {
            this.mathsize = this.parent.mathsize;
            this.fontSize = this.parent.fontSize;
            this.metriclist = this.parent.metriclist;
            this.scriptlevel = this.parent.scriptlevel;
            this.tightspaces = this.parent.tightspaces;
            this.displaystyle = this.parent.displaystyle;
            this.color = this.parent.color;
            this.fontfamilies = this.parent.fontfamilies;
            this.fontweight = this.parent.fontweight;
            this.fontstyle = this.parent.fontstyle;
            this.defaults = this.parent.defaults;
            this.parent.makeChildContext(this);
        }
        else {
            this.mathsize = this.parseLength(this.defaults['mathsize']);
            this.fontSize = this.mathsize;
            this.metriclist = null;
            this.scriptlevel = this.parseInt(this.defaults['scriptlevel']);
            this.tightspaces = false;
            this.displaystyle = (this.defaults['displaystyle'] == 'true');
            this.color = this.defaults['mathcolor'];

            var mv = this.config.variants[this.defaults['mathvariant']];
            this.fontweight = mv[0];
            this.fontstyle = mv[1];
            this.fontfamilies = mv[2];
        }

        this.processFontAttributes();

        this.width = 0;
        this.height = 0;
        this.depth = 0;
        this.ascender = 0;
        this.descender = 0;
        this.leftspace = 0;
        this.rightspace = 0;
        this.alignToAxis = false;
        this.base = this;
        this.core = this;
        this.stretchy = false;
        this.accent = false;
        this.moveLimits = false;
        this.textShift = 0;
        this.textStretch = 1;
        this.leftBearing = 0;
        this.rightBearing = 0;
        this.isSpace = false;
        this.metriclist = null;
        this.nominalMetric = null;
    };

    MathNode.prototype.context_math = function() {
        this.default_context();
        var attr = this.getAttribute('display');
        if(attr) {
            this.displaystyle = (attr == 'block');
        }
        else {
            attr = this.getAttribute('mode');
            if(attr) {
                this.displaystyle = (attr == 'display');
            }
            else {
                this.displaystyle = false;
            }
        }
    };

    MathNode.prototype.context_mstyle = function() {
        this.default_context();
        if(this.element.hasAttribute('mathsize')) this.element.removeAttribute('mathsize');
        this.defaults = JSON.parse(JSON.stringify(this.defaults));
        for(var i = 0; i < this.element.attributes.length; i++) {
            var attr = this.element.attributes[i];
            this.defaults[attr.name] = attr.value;
        }
    };

    MathNode.prototype.context_mtable = function() {
        this.default_context();
        this.displaystyle = (this.getProperty('displaystyle') == 'true');
    };

    MathNode.prototype.context_mi = function() {
        if(this.text.length == 1) {
            if(!this.element.hasAttribute('fontstyle')) this.element.setAttribute('fontstyle', 'italic');
        }
        this.default_context();
    };

    MathNode.prototype.context_mo = function() {
        var extra_style = this.config.opstyles[this.text];
        if(extra_style) {
            for(var key in extra_style) {
                if(!this.element.hasAttribute(key)) this.element.setAttribute(key, extra_style[key]);
            }
        }

        var form = 'infix';
        var ptypes = ['mrow', 'mstyle', 'msqrt', 'merror', 'mpadded', 'mphantom', 'menclose', 'mtd', 'math'];
        if(this.parent && ptypes.indexOf(this.parent.element.tagName) >= 0) {
            var prevSiblings = [];
            var nextSiblings = [];
            for(var i = 0; i < this.parent.children.length; i++) {
                var sibling = this.parent.children[i];
                if(i < this.nodeIndex && sibling.element.tagName != 'mspace') {
                    prevSiblings.push(sibling);
                }
                else if(i > this.nodeIndex && sibling.element.tagName != 'mspace') {
                    nextSiblings.push(sibling);
                }
            }

            if(prevSiblings.length === 0 && nextSiblings.length > 0) form = 'prefix';
            else if(nextSiblings.length === 0 && prevSiblings.length > 0) form = 'postfix';
        }

        form = this.getAttribute('form') || form;

        this.opdefaults = this.config.operators.lookup(this.text, form);
        this.default_context();
        this.stretchy = this.getProperty('stretchy', this.opdefaults['stretchy']) == 'true';
        this.symmetric = this.getProperty('symmetric', this.opdefaults['symmetric']) == 'true';
        this.scaling = this.opdefaults['scaling'];
        if(!this.tightspaces) {
            this.leftspace = this.parseSpace(this.getProperty('lspace', this.opdefaults['lspace']));
            this.rightspace = this.parseSpace(this.getProperty('rspace', this.opdefaults['rspace']));
        }

        if(this.displaystyle) {
            if(this.getProperty('largeop', this.opdefaults['largeop']) == 'true') this.fontSize *= 1.41;
        }
        else {
            this.moveLimits = this.getProperty('movablelimits', this.opdefaults['movablelimits']) == 'true';
        }
    };

    MathNode.prototype.processFontAttributes = function() {
        var attr = this.getAttribute('displaystyle');
        if(attr !== null && attr !== undefined) {
            this.displaystyle = (attr == 'true');
        }

        var scriptlevelattr = this.getAttribute('scriptlevel');
        if(scriptlevelattr !== null && scriptlevelattr !== undefined) {
            scriptlevelattr = scriptlevelattr.trim();
            if(scriptlevelattr.match(/^\+/)) {
                this.scriptlevel += this.parseInt(scriptlevelattr.substr(1));
            }
            else if(scriptlevelattr.match(/^\-/)) {
                this.scriptlevel -= this.parseInt(scriptlevelattr.substr(1));
            }
            else {
                this.scriptlevel = this.parseInt(scriptlevelattr);
            }
            this.scriptlevel = Math.max(this.scriptlevel, 0);
        }

        this.color = this.getAttribute('mathcolor') || this.getAttribute('color') || this.color;

        var mathvariantattr = this.getAttribute('mathvariant');
        if(mathvariantattr !== null && mathvariantattr !== undefined) {
            var mathvariant = this.config.variants[mathvariantattr];
            if(mathvariant === null || mathvariant === undefined) {
                this.error("Ignored mathvariant attribute: value '" + mathvariantattr + "' not defined in the font configuration file");
            }
            else {
                this.fontweight = mathvariant[0];
                this.fontstyle = mathvariant[1];
                this.fontfamilies = mathvariant[2];
            }
        }
        else {
            this.fontweight = this.getAttribute('fontweight') || this.fontweight;
            this.fontstyle = this.getAttribute('fontstyle') || this.fontstyle;
            var familyattr = this.getAttribute('fontfamily');
            if(familyattr !== null && familyattr !== undefined) {
                this.fontfamilies = [];
                var ff = familyattr.split(',');
                for(var i = 0; i < ff.length; i++) {
                    this.fontfamilies.push(ff[i]);
                }
            }
        }

        var mathsizeattr = this.getAttribute('mathsize');
        if(mathsizeattr !== null && mathsizeattr !== undefined) {
            if(mathsizeattr == 'normal') this.mathsize = this.parseLength(this.defaults['mathsize']);
            else if(mathsizeattr == 'big') this.mathsize = this.parseLength(this.defaults['mathsize']) * 1.41;
            else if(mathsizeattr == 'small') this.mathsize = this.parseLength(this.defaults['mathsize']) / 1.41;
            else {
                var mathsize = this.parseLengthOrPercent(mathsizeattr, this.mathsize);
                if(mathsize > 0) this.mathsize = mathsize;
                else this.error("Value of attribute 'mathsize' ignored - not a positive length: " + mathsizeattr);
            }
        }

        this.fontSize = this.mathsize;
        if(this.scriptlevel > 0) {
            var scriptsizemultiplier = this.parseFloat(this.defaults['scriptsizemultiplier']);
            if(scriptsizemultiplier <= 0) {
                this.error("Bad inherited value of 'scriptsizemultiplier' attribute: " + mathsizeattr + "; using default value");
                scriptsizemultiplier = this.parseFloat(this.globalDefaults['scriptsizemultiplier']);
            }
            this.fontSize *= Math.pow(scriptsizemultiplier, this.scriptlevel);
        }

        var fontsizeattr = this.getAttribute('fontsize');
        if((fontsizeattr !== null && fontsizeattr !== undefined) && (mathsizeattr === null || mathsizeattr === undefined)) {
            var fontSizeOverride = this.parseLengthOrPercent(fontsizeattr, this.fontSize);
            if(fontSizeOverride > 0) {
                this.mathsize *= (fontSizeOverride / this.fontSize);
                this.fontSize = fontSizeOverride;
            }
            else {
                this.error("Value of attribute 'fontsize' ignored - not a positive length: " + fontsizeattr);
            }
        }

        var scriptminsize = this.parseLength(this.defaults['scriptminsize']);
        this.fontSize = Math.max(this.fontSize, scriptminsize);
        this.originalFontSize = this.fontSize;
    };

    MathNode.prototype.default_child_context = function(child) {};

    MathNode.prototype.child_context_mfrac = function(child) {
        if(this.displaystyle) {
            child.displaystyle = false;
        }
        else {
            child.scriptlevel += 1;
        }
    };

    MathNode.prototype.child_context_mroot = function(child) {
        if(child.nodeIndex == 1) {
            child.displaystyle = false;
            child.scriptlevel += 2;
            child.tightspaces = true;
        }
    };

    MathNode.prototype.child_context_msub = function(child) {
        this.makeScriptContext(child);
    };

    MathNode.prototype.child_context_msup = function(child) {
        this.makeScriptContext(child);
    };

    MathNode.prototype.child_context_msubsup = function(child) {
        this.makeScriptContext(child);
    };

    MathNode.prototype.child_context_mmultiscripts = function(child) {
        this.makeScriptContext(child);
    };

    MathNode.prototype.child_context_munder = function(child) {
        if(child.nodeIndex == 1) this.makeLimitContext(child, 'accentunder');
    };

    MathNode.prototype.child_context_mover = function(child) {
        if(child.nodeIndex == 1) this.makeLimitContext(child, 'accent');
    };

    MathNode.prototype.child_context_munderover = function(child) {
        if(child.nodeIndex == 1) this.makeLimitContext(child, 'accentunder');
        else if(child.nodeIndex == 2) this.makeLimitContext(child, 'accent');
    };

    MathNode.prototype.makeScriptContext = function(child) {
        if(child.nodeIndex > 0) {
            child.displaystyle = false;
            child.tightspaces = true;
            child.scriptlevel += 1;
        }
    };

    MathNode.prototype.makeLimitContext = function(child, accentProperty) {
        child.displaystyle = false;
        child.tightspaces = true;

        var accentValue = this.getProperty(accentProperty);
        if(accentValue === null || accentValue === undefined) {
            var embellishments = ['msub', 'msup', 'msubsup', 'munder', 'mover', 'munderover', 'mmultiscripts'];

            var getAccentValue = function(ch) {
                if(ch.element.tagName == 'mo') return ch.opdefaults['accent'];
                else if(embellishments.indexOf(child.element.tagName) >= 0 && child.children.length > 0) {
                    return getAccentValue(ch.children[0]);
                }
                else return "false";
            };

            accentValue = getAccentValue(child);
        }

        child.accent = (accentValue == 'true');
        if(!child.accent) child.scriptlevel += 1;
    };

    MathNode.prototype.addRadicalEnclosure = function() {
        this.lineWidth = this.nominalThinStrokeWidth();
        this.thickLineWidth = this.nominalThickStrokeWidth();
        this.gap = this.nominalLineGap();
        if(!this.displaystyle) this.gap /= 2.0;

        this.rootHeight = Math.max(this.base.height, this.base.ascender, this.nominalAscender());
        this.rootHeight += this.gap + this.lineWidth;
        this.height = this.rootHeight;

        this.alignToAxis = this.base.alignToAxis;

        // if(this.alignToAxis) {
            this.rootDepth = Math.max(0, this.base.depth - this.lineWidth);
            this.depth = Math.max(this.base.depth, this.rootDepth + this.lineWidth);
        // }
        // else {
        //     this.rootDepth = 0;
        //     this.depth = Math.max(this.base.depth, this.lineWidth);
        // }

        this.rootWidth = (this.rootHeight + this.rootDepth) * 0.6;
        this.cornerWidth = this.rootWidth * 0.9 - this.gap - this.lineWidth / 2.0;
        this.cornerHeight = (this.rootHeight + this.rootDepth) * 0.5 - this.gap - this.lineWidth / 2.0;

        this.width = this.base.width + this.rootWidth  + 2 * this.gap;
        this.ascender = this.height;
        this.descender = this.base.descender;
        this.leftspace = this.lineWidth;
        this.rightspace = this.lineWidth;
    };

    MathNode.prototype.addLongDivisionEnclosure = function() {
        this.lineWidth = this.nominalThinStrokeWidth();
        this.gap = this.nominalLineGap();
        if(!this.displaystyle) this.gap /= 2.0;

        this.rootHeight = Math.max(this.base.height, this.base.ascender, this.nominalAscender());
        this.rootHeight += this.gap + this.lineWidth;
        this.height = this.rootHeight;

        this.alignToAxis = this.base.alignToAxis;

        if(this.alignToAxis) {
            this.rootDepth = Math.max(0, this.base.depth - this.lineWidth);
            this.depth = Math.max(this.base.depth, this.rootDepth + this.lineWidth);
        }
        else {
            this.rootDepth = 0;
            this.depth = Math.max(this.base.depth, this.lineWidth);
        }

        this.rootWidth = (this.rootHeight + this.rootDepth) * 0.6;

        this.width = this.base.width + this.rootWidth  + 2 * this.gap;
        this.ascender = this.height;
        this.descender = this.base.descender;
        this.leftspace = this.lineWidth;
        this.rightspace = this.lineWidth;
    };

    MathNode.prototype.addBoxEnclosure = function() {
        this.width += 2 * this.hdelta;
        this.height += this.vdelta;
        this.depth += this.vdelta;
        this.ascender = this.base.ascender;
        this.descender = this.base.descender;
    };

    MathNode.prototype.addCircleEnclosure = function() {
        this.width += this.hdelta / 2;
        this.height += this.vdelta / 4;
        this.depth += this.vdelta / 4;
        var height = this.height + this.depth;
        var ratio;

        if(this.width > height) {
            this.ry = Math.sqrt(Math.pow(height/2.0, 2) * 2);
            ratio = (this.width * 1.0) / height;
            this.rx = this.ry * ratio;
        }
        else {
            this.rx = Math.sqrt(Math.pow(this.width/2.0, 2) * 2);
            ratio = (height * 1.0) / this.width;
            this.ry = this.rx * ratio;
        }

        this.cy = -((this.height - this.depth) / 2.0);
        this.width = this.rx * 2.0;
        this.cx = this.width / 2.0;
        this.height = this.ry - this.cy;
        this.depth = this.ry + this.cy;
        this.rx -= (this.borderWidth / 2.0);
        this.ry -= (this.borderWidth / 2.0);
        this.ascender = this.base.ascender;
        this.descender = this.base.descender;
    };

    MathNode.prototype.addBorderEnclosure = function(borders) {
        if(borders[0]) this.width += this.hdelta;
        if(borders[1]) this.width += this.hdelta;
        if(borders[2]) this.height += this.vdelta;
        if(borders[3]) this.depth += this.vdelta;
        this.decorationData = borders;
        this.ascender = this.base.ascender;
        this.descender = this.base.descender;
    };

    MathNode.prototype.default_measure = function() {};

    MathNode.prototype.measure_none = function() {};

    MathNode.prototype.measure_mprescripts = function() {};

    MathNode.prototype.measure_math = function() {
        this.measure_mrow();
        var spacing = this.parseSpace('thinmathspace');
        this.height += spacing;
        this.depth += spacing;
        this.width += spacing * 2;
        
        var ch0 = this;
        while(ch0.children.length > 0 && (ch0.element.tagName == 'math' || ch0.element.tagName == 'mrow')) {
            ch0 = ch0.children[0];
        }

        var old_children = this.children;
        this.children = [];
        this.createNode('mspace', {});
        this.children[0].measure();
        this.children[0].width = spacing;
        this.children = this.children.concat(old_children);
        
        if(ch0.leftBearing > 0) {
            this.children[0].width += ch0.leftBearing;
            this.width += ch0.leftBearing;
        }

        var ch1 = this;
        while(ch1.children.length > 0 && (ch1.element.tagName == 'math' || ch1.element.tagName == 'mrow')) {
            ch1 = ch1.children[ch1.children.length - 1];
        }
        if(ch1.rightBearing > 0) {
            this.width += ch1.rightBearing;
        }
    };

    MathNode.prototype.measure_mphantom = function() {
        this.measure_mrow();
    };

    MathNode.prototype.measure_mstyle = function() {
        this.measure_mrow();
    };

    MathNode.prototype.measure_maction = function() {
        var selectionattr = this.getAttribute('selection');
        var seletion = this.parseInt(selectionattr);
        this.base = null;

        if(seletion <= 0) {
            this.error("Invalid value '" + selectionattr + "' for 'selection' attribute - not a positive integer");
        }
        else if(this.children.length === 0) {
            this.error("No valid subexpression inside maction element - element ignored");
        }
        else {
            if(seletion > this.children.length) {
                this.error("Invalid value '" + seletion + "' for 'selection' attribute - there are only " + this.children.length + " expression descendants in the element");
                selection = 1;
            }

            this.setNodeBase(this.children[seletion - 1]);
            this.width = this.base.width;
            this.height = this.base.height;
            this.depth = this.base.depth;
            this.ascender = this.base.ascender;
            this.descender = this.base.descender;
        }
    };

    MathNode.prototype.measure_mpadded = function() {
        this.createImplicitRow();

        var parseDimension = function(attr, startvalue, canUseSpaces) {
            var basevalue;
            if(attr.match(/ height$/)) {
                basevalue = this.base.height;
                attr = attr.substr(0, attr.length - 7);
            }
            else if(attr.match(/ depth$/)) {
                basevalue = this.base.depth;
                attr = attr.substr(0, attr.length - 6);
            }
            else if(attr.match(/ width$/)) {
                basevalue = this.base.width;
                attr = attr.substr(0, attr.length - 6);
            }
            else {
                basevalue = startvalue;
            }

            if(attr.match(/%$/)) {
                attr = attr.substr(0, attr.length - 1);
                basevalue /= 100.0;
            }

            if(canUseSpaces) {
                return this.parseSpace(attr, basevalue);
            }
            else {
                return this.parseLength(attr, basevalue);
            }
        };

        var getDimension = function(attname, startvalue, canUseSpaces) {
            var attr = this.getAttribute(attname);
            if(attr === undefined || attr === null) return startvalue;

            attr = attr.replace(/ +/g, ' ').trim();
            if(attr.match(/^\+/)) {
                return startvalue + parseDimension.call(this, attr.substr(1), startvalue, canUseSpaces);
            }
            else if(attr.match(/^\-/)) {
                return startvalue - parseDimension.call(this, attr.substr(1), startvalue, canUseSpaces);
            }
            else {
                return parseDimension.call(this, attr, startvalue, canUseSpaces);
            }
        };

        this.height = getDimension.call(this, 'height', this.base.height, false);
        this.depth = getDimension.call(this, 'depth', this.base.height, false);
        this.ascender = this.base.ascender;
        this.descender = this.base.descender;
        this.leftpadding = getDimension.call(this, 'lspace', 0, true);
        this.width = getDimension.call(this, 'width', this.base.width + this.leftpadding, true);
        if(this.width < 0) this.width = 0;
        this.leftspace = this.base.leftspace;
        this.rightspace = this.base.rightspace;
    };

    MathNode.prototype.measure_mfenced = function() {
        var old_children = this.children;
        this.children = [];

        var openingFence = normalize_string(this.getProperty('open'));
        if(openingFence.length > 0) {
            var opening = this.createNode('mo', { fence: 'true', form: 'prefix' });
            opening.text = openingFence;
            opening.measure();
        }

        var separators = this.getProperty('separators').replace(/ /g, '');
        var sepindex = 0;
        var lastsep = separators.length - 1;
        var addSeparators = false;

        for(var i = 0; i < old_children.length; i++) {
            var ch = old_children[i];
            if(addSeparators && lastsep >= 0) {
                var sep = this.createNode('mo', { separator: 'true', form: 'infix' });
                sep.text = separators.charAt(sepindex);
                sep.measure();
                sepindex = Math.min(sepindex + 1, lastsep);
                addSeparators = true;
            }
            this.children.push(ch);
        }

        var closingFence = normalize_string(this.getProperty('close'));
        if(closingFence.length > 0) {
            var closeing = this.createNode('mo', { fence: 'true', 'form': 'postfix' });
            closeing.text = closingFence;
            closeing.measure();
        }

        this.measure_mrow();
    };

    MathNode.prototype.measure_mo = function() {
        if(this.hasGlyph(0x2212)) this.text = this.text.replace(/-/g, '\u2212');
        if(this.hasGlyph(0x2032)) this.text = this.text.replace(/'/g, '\u2032');
        if(['\u2061', '\u2062', '\u2063'].indexOf(this.text) >= 0) this.isSpace = true;
        else this.measureText();

        this.alignToAxis = true;
        this.textShift = -this.axis();
        this.height += this.textShift;
        this.ascender += this.textShift;
        this.depth -= this.textShift;
        this.descender -= this.textShift;
    };

    MathNode.prototype.measure_mn = function() {
        this.measureText();
    };

    MathNode.prototype.measure_mi = function() {
        this.measureText();
    };

    MathNode.prototype.measure_mtext = function() {
        this.measureText();
    };

    MathNode.prototype.measure_merror = function() {
        this.createImplicitRow();
        this.borderWidth = this.nominalLineWidth();
        this.width = this.base.width + 2 * this.borderWidth;
        this.height = this.base.height + this.borderWidth;
        this.depth = this.base.depth + this.borderWidth;
        this.ascender = this.base.ascender;
        this.descender = this.base.descender;
    };

    MathNode.prototype.measure_ms = function() {
        var lq = this.getProperty('lquote');
        var rq = this.getProperty('rquote');

        if(lq) this.text = this.text.replace(lq, '\\' + lq);
        if(rq) this.text = this.text.replace(rq, '\\' + rq);

        this.text = lq + this.text + rq;
        this.measureText();
        var spacing = this.parseSpace('thinmathspace');
        this.leftspace = spacing;
        this.rightspace = spacing;
    };

    MathNode.prototype.measure_mspace = function() {
        this.height = this.parseLength(this.getProperty('height'));
        this.depth = this.parseLength(this.getProperty('depth'));
        this.width = this.parseLength(this.getProperty('width'));

        this.ascender = this.nominalAscender();
        this.descender = this.nominalDescender();
    };

    MathNode.prototype.measure_mrow = function() {
        if(this.children.length === 0) return;
        var i; var ch;

        this.alignToAxis = true;
        this.isSpace = true;
        for(i = 0; i < this.children.length; i++) {
            ch = this.children[i];
            if(!ch.isSpace) {
                this.isSpace = false;
                this.alignToAxis = this.alignToAxis && ch.alignToAxis;
            }
        }

        var longtext = function(n) {
            if(n === null || n === undefined) return false;
            if(n.isSpace) return false;
            if(n.core.element.tagName == 'ms') return true;
            if(['mi', 'mo', 'mtext'].indexOf(n.core.element.tagName) >= 0) return (n.core.text.length > 1);
            return false;
        };

        for(i = 0; i < this.children.length; i++) {
            ch = this.children[i];
            if(ch.core.element.tagName != 'mo') continue;
            if(['\u2061', '\u2062', '\u2063'].indexOf(ch.text) >= 0) {
                ch.text = '';
                var ch_prev; var ch_next;
                if(i > 0) ch_prev = this.children[i-1];
                if(i + 1 < this.children.length) ch_next = this.children[i+1];
                if(longtext(ch_prev) || longtext(ch_next)) ch.width = ch.parseSpace('thinmathspace');
            }
        }

        var vse = this.getVerticalStretchExtent(this.children, this.alignToAxis, this.axis());
        this.ascender = vse[0];
        this.descender = vse[1];
        for(i = 0; i < this.children.length; i++) {
            ch = this.children[i];
            if(ch.core.stretchy) {
                var desiredHeight = this.ascender;
                var desiredDepth = this.descender;
                if(ch.alignToAxis && !this.alignToAxis) {
                    desiredHeight -= this.axis();
                    desiredDepth += this.axis();
                }
                desiredHeight -= ch.core.ascender - ch.core.height;
                desiredDepth -= ch.core.descender - ch.core.depth;
                ch.stretch(null, desiredHeight, desiredDepth, this.alignToAxis);
            }
        }

        var rve = this.getRowVerticalExtent(this.children, this.alignToAxis, this.axis());
        this.height = rve[0];
        this.depth = rve[1];
        this.ascender = rve[2];
        this.descender = rve[3];

        this.width += this.children.reduce(function(sum, ch) { return sum + ch.width + ch.leftspace + ch.rightspace; }, 0);

        this.leftspace = this.children[0].leftspace;
        this.rightspace = this.children[this.children.length - 1].rightspace;
        this.width -= this.leftspace + this.rightspace;
    };

    MathNode.prototype.measure_mfrac = function() {
        if(this.children.length != 2) {
            this.error("Invalid content of 'mfrac' element: element should have exactly two children");
            if(this.children.length < 2) {
                this.measure_mrow();
                return;
            }
        }

        this.enumerator = this.children[0];
        this.denominator = this.children[1];

        this.alignToAxis = true;

        var ruleWidthKeywords = { 'medium': '1', 'thin': '0.5', 'thick': '2' };

        var widthAttr = this.getProperty('linethickness');
        widthAttr = ruleWidthKeywords[widthAttr] || widthAttr;
        var unitWidth = this.nominalLineWidth();
        this.rulewidth = this.parseLength(widthAttr, unitWidth);

        this.ruleGap = this.nominalLineGap();
        if(this.tightspaces) this.ruleGap /= 1.41;

        if(this.getProperty('bevelled') == 'true') {
            var eh = this.enumerator.height + this.enumerator.depth;
            var dh = this.denominator.height + this.denominator.depth;

            var vshift = Math.min(eh, dh) / 2.0;

            this.height = (eh + dh - vshift) / 2.0;
            this.depth = this.height;
            this.slope = defaultSlope;
            this.width = this.enumerator.width + this.denominator.width;
            this.width += vshift / slope;
            this.width += (this.rulewidth + this.ruleGap) * Math.sqrt(1 + Math.pow(this.slope, 2));
            this.leftspace = this.enumerator.leftspace;
            this.rightspace = this.enumerator.rightspace;
        }
        else {
            this.height = this.rulewidth / 2.0 + this.ruleGap + this.enumerator.height + this.enumerator.depth;
            this.depth = this.rulewidth / 2.0 + this.ruleGap + this.denominator.height + this.denominator.depth;
            this.width = Math.max(this.enumerator.width, this.denominator.width) + 2 * this.rulewidth;
            this.leftspace = this.rulewidth;
            this.rightspace = this.rulewidth;
        }

        this.ascender = this.height;
        this.descender = this.depth;
    };

    MathNode.prototype.measure_msqrt = function() {
        this.createImplicitRow();
        this.addRadicalEnclosure();
    };

    MathNode.prototype.measure_mroot = function() {
        if(this.children.length != 2) {
            this.error("Invalid content of 'mroot' element: element should have exactly two children");
        }

        if(this.children.length < 2) {
            this.rootindex = null;
            this.measure_msqrt();
        }
        else {
            this.setNodeBase(this.children[0]);
            this.rootindex = this.children[1];
            this.addRadicalEnclosure();
            this.width += Math.max(0, this.rootindex.width - this.cornerWidth);
            this.height += Math.max(0, this.rootindex.height + this.rootindex.depth - this.cornerHeight);
            this.ascender = this.height;
        }
    };

    MathNode.prototype.measure_msub = function() {
        if(this.children.length != 2) {
            this.error("Invalid content of 'msub' element: element should have exactly two children");
            if(this.children.length < 2) {
                this.measure_mrow();
                return;
            }
        }

        this.measureScripts([this.children[1]], null);
    };

    MathNode.prototype.measure_msup = function() {
        if(this.children.length != 2) {
            this.error("Invalid content of 'msup' element: element should have exactly two children");
            if(this.children.length < 2) {
                this.measure_mrow();
                return;
            }
        }

        this.measureScripts(null, [this.children[1]]);
    };

    MathNode.prototype.measure_msubsup = function() {
        if(this.children.length != 3) {
            this.error("Invalid content of 'msubsup' element: element should have exactly three children");
            if(this.children.length == 2) {
                this.measure_msub();
                return;
            }
            else if(this.children.length < 2) {
                this.measure_mrow();
                return;
            }
        }

        this.measureScripts([this.children[1]], [this.children[2]]);
    };

    MathNode.prototype.measure_munder = function() {
        if(this.children.length != 2) {
            this.error("Invalid content of 'munder' element: element should have exactly two children");
            if(this.error.length < 2) {
                this.measure_mrow();
                return;
            }
        }

        this.measureLimits(this.children[1], null);
    };

    MathNode.prototype.measure_mover = function() {
        if(this.children.length != 2) {
            this.error("Invalid content of 'mover' element: element should have exactly two children");
            if(this.error.length < 2) {
                this.measure_mrow();
                return;
            }
        }

        this.measureLimits(null, this.children[1]);
    };

    MathNode.prototype.measure_munderover = function() {
        if(this.children.length != 3) {
            this.error("Invalid content of 'munderover' element: element should have exactly three children");
            if(this.children.length == 2) {
                this.measure_munder();
                return;
            }
            else if(this.children.length < 2) {
                this.measure_mrow();
                return;
            }
        }

        this.measureLimits(this.children[1], this.children[2]);
    };

    MathNode.prototype.measure_mmultiscripts = function() {
        if(this.children.length === 0) {
            this.measure_mrow();
            return;
        }

        var subscripts = [];
        var superscripts = [];
        var presubscripts = [];
        var presuperscripts = [];

        var isPre = false;
        var isSub = true;

        for(var i = 1; i < this.children.length; i++) {
            var ch = this.children[i];
            if(ch.element.tagName == 'mprescripts') {
                if(isPre) this.error("Repeated 'mprescripts' element inside 'mmultiscripts\n");
                isPre = true;
                isSub = true;
                continue;
            }

            if(isSub) {
                if(isPre) presubscripts.push(ch);
                else subscripts.push(ch);
            }
            else {
                if(isPre) presuperscripts.push(ch);
                else superscripts.push(ch);
            }

            isSub = !isSub;
        }

        this.measureScripts(subscripts, superscripts, presubscripts, presuperscripts);
    };

    MathNode.prototype.measure_menclose = function() {
        var pushEnclosure = function() {
            if(this.decoration === null || this.decoration === undefined) return;

            this.wrapChildren('menclose');
            this.children[0].setNodeBase(this.base);
            this.setNodeBase(this.children[0]);
            this.base.decoration = this.decoration;
            this.base.decorationData = this.decorationData;
            this.decoration = null;
            this.decorationData = null;
            this.base.width = this.width;
            this.base.height = this.height;
            this.base.depth = this.depth;
            this.base.borderWidth = this.borderWidth;
            this.base.ascender = this.ascender;
            this.base.descender = this.descender;
            this.base.color = this.color;
        };

        this.createImplicitRow();
        var signs = this.getProperty('notation').split(' ');
        this.width = this.base.width;
        this.height = this.base.height;
        this.depth = this.base.depth;
        this.decoration = null;
        this.decorationData = null;
        this.borderWidth = this.nominalLineWidth();
        this.hdelta = this.nominalLineGap() + this.borderWidth;
        this.vdelta = this.nominalLineGap() + this.borderWidth;

        if(signs.indexOf('radical') >= 0) {
            this.wrapChildren('msqrt');
            this.children[0].setNodeBase(this.base);
            this.setNodeBase(this.children[0]);
            this.base.makeContext();
            this.base.measureNode();
            this.width = this.base.width;
            this.height = this.base.height;
            this.depth = this.base.depth;
        }

        var strikes = [(signs.indexOf('horizontalstrike') >= 0), (signs.indexOf('verticalstrike') >= 0), (signs.indexOf('updiagonalstrike') >= 0), (signs.indexOf('downdiagonalstrike') >= 0)];
        if(strikes.indexOf(true) >= 0) {
            pushEnclosure.call(this);
            this.decoration = 'strikes';
            this.decorationData = strikes;
        }

        if(signs.indexOf('roundedbox') >= 0) {
            pushEnclosure.call(this);
            this.decoration = 'roundedbox';
            this.addBoxEnclosure();
        }

        if(signs.indexOf('box') >= 0) {
            pushEnclosure.call(this);
            this.decoration = 'box';
            this.addBoxEnclosure();
        }

        if(signs.indexOf('circle') >= 0) {
            pushEnclosure.call(this);
            this.decoration = 'circle';
            this.addCircleEnclosure();
        }

        var borders = [(signs.indexOf('left') >= 0), (signs.indexOf('right') >= 0), (signs.indexOf('top') >= 0), (signs.indexOf('bottom') >= 0)];
        if(borders.indexOf(true) >= 0) {
            pushEnclosure.call(this);
            if(borders.indexOf(false) >= 0) {
                this.decoration = 'borders';
                this.addBorderEnclosure(borders);
            }
            else {
                this.decoration = 'box';
                this.addBoxEnclosure();
            }
        }

        if(signs.indexOf('longdiv') >= 0) {
            pushEnclosure.call(this);
            this.decoration = 'longdiv';
            this.addLongDivisionEnclosure();
        }

        if(signs.indexOf('actuarial') >= 0) {
            pushEnclosure.call(this);
            this.decoration = 'borders';
            this.addBorderEnclosure([false, true, true, false]);
        }
    };

    MathNode.prototype.measure_mtable = function() {
        this.lineWidth = this.nominalLineWidth();
        this.arrangeCells();
        this.arrangeLines();
        this.calculateColumnWidths();

        var i; var j; var k; var r; var c; var content; var cellSize;

        for(i = 0; i < this.rows.length; i++) {
            r = this.rows[i];
            for(j = 0; j < r.cells.length; j++) {
                c = r.cells[j];

                if(c === null || c === undefined || c.content === null || c.contet === undefined) continue;
                content = c.content;
                if(content.element.tagName == 'mtd') {
                    if(content.children.length != 1) continue;
                    content = content.children[0];
                    if(content.core.stretchy) c.content = content;
                }
                if(content.core.stretchy) {
                    if(c.colspan == 1) {
                        content.stretch(this.columns[j].width);
                    }
                    else {
                        cellSize = 0;
                        for(k = j; k < j + c.colspan; k++) {
                            cellSize += this.columns[k].width + this.columns[k].spaceAfter;
                        }
                        cellSize -= this.columns[j + c.colspan].spaceAfter;
                        content.stretch(cellSize);
                    }
                }
            }
        }

        this.calculateRowHeights();

        for(i = 0; i < this.rows.length; i++) {
            r = this.rows[i];
            for(j = 0; j < r.cells.length; j++) {
                c = r.cells[j];
                
                if(c === null || c === undefined || c.content === null || c.contet === undefined) continue;
                content = c.content;
                if(content.element.tagName == 'mtd') {
                    if(content.children.length != 1) continue;
                    content = content.children[0];
                    if(content.core.stretchy) c.content = content;
                }
                if(content.core.stretchy) {
                    if(c.rowspan == 1) {
                        content.stretch(null, r.height - c.vshift, r.depth + c.vshift);
                    }
                    else {
                        cellSize = 0;
                        for(k = j; k < j + c.rowspan; k++) {
                            cellSize += this.rows[k].height + this.rows[k].depth + this.rows[k].spaceAfter;
                        }
                        cellSize -= this.rows[j + c.rowspan].spaceAfter;
                        content.stretch(null, cellSize / 2.0, cellSize / 2.0);
                    }
                }
            }
        }

        this.calculateColumnWidths();

        this.width = this.columns.reduce(function(sum, col) { return sum + col.width + col.spaceAfter; }, 0);
        this.width += 2 * this.framespacings[0];

        var vsize = this.rows.reduce(function(sum, row) { return sum + row.height + row.depth + row.spaceAfter; }, 0);
        vsize += 2 * this.framespacings[1];

        var a = this.getAlign();
        var alignType = a[0];
        var alignRow = a[1];

        var topLine; var bottomLine; var axisLine; var baseLine;
        if(alignRow === null || alignRow === undefined) {
            topLine = 0;
            bottomLine = vsize;
            axisLine = vsize / 2.0;
            baseLine = axisLine + this.axis();
        }
        else {
            var row = this.rows[alignRow - 1];
            topLine = this.framespacings[1];
            for(i = 0; i < alignRow; i++) {
                r = this.rows[i];
                topLine += r.height + r.depth + r.spaceAfter;
            }
            bottomLine = topLine + row.height + row.depth;
            if(row.alignToAxis) {
                axisLine = topLine + row.height;
                baseLine = axisLine + this.axis();
            }
            else {
                baseLine = topLine + row.height;
                axisLine = baseLine - this.axis();
            }
        }

        if(alignType == 'axis') {
            this.alignToAxis = true;
            this.height = axisLine;
        }
        else if(alignType == 'baseline') {
            this.alignToAxis = false;
            this.height = baseLine;
        }
        else if(alignType == 'center') {
            this.alignToAxis = false;
            this.height = (topLine + bottomLine) / 2.0;
        }
        else if(alignType == 'top') {
            this.alignToAxis = false;
            this.height = topLine;
        }
        else if(alignType == 'bottom') {
            this.alignToAxis = false;
            this.height = bottomLine;
        }
        else {
            this.error("Unrecognized or unsupported table alignment value: " + alignType);
            this.alignToAxis = true;
            this.height = axisLine;
        }

        this.depth = vsize - this.height;

        this.ascender = this.height;
        this.descender = this.depth;
    };

    MathNode.prototype.measure_mtr = function() {
        if(!this.parent || this.parent.element.tagName != 'mtable') {
            this.error("Misplaced '" + this.element.tagName + "' element: should be child of 'mtable'");
        }
    };

    MathNode.prototype.measure_mlabeledtr = function() {
        if(this.children.length === 0) {
            this.error("Missing label in 'mlabeledtr' element");
        }
        else {
            this.warning("MathML element 'mlabeledtr' is unsupported: label omitted");
            this.children.shift();
        }
        this.measure_mtr();
    };

    MathNode.prototype.measure_mtd = function() {
        if(!this.parent || ['mtable', 'mtr', 'mlabeledtr'].indexOf(this.parent.element.tagName) == -1) {
            this.error("Misplaced 'mtd' element: should be child of 'mtr', 'mlabeledtr', or 'mtable'");
        }
        this.measure_mrow();
    };

    MathNode.prototype.measureScripts = function(subscripts, superscripts, presubscripts, presuperscripts) {
        this.subscripts = subscripts || [];
        this.superscripts = superscripts || [];
        this.presubscripts = presubscripts || [];
        this.presuperscripts = presuperscripts || [];

        this.setNodeBase(this.children[0]);
        this.width = this.base.width;
        this.height = this.base.height;
        this.depth = this.base.depth;
        this.ascender = this.base.ascender;
        this.descender = this.base.descender;

        var subs = this.subscripts.concat(this.presubscripts);
        var supers = this.superscripts.concat(this.presuperscripts);
        var allscripts = subs.concat(supers);

        var max_axis = function(max, x) { return Math.max(max, x.axis()); };
        var max_lineGap = function(max, x) { return Math.max(max, x.nominalLineGap()); };

        this.subscriptAxis = subs.reduce(max_axis, 0);
        this.superscriptAxis = supers.reduce(max_axis, 0);
        var gap = allscripts.reduce(max_lineGap, -1000000);
        var protrusion = this.parseLength('0.25ex');
        var scriptMedian = this.axis();

        var rve = this.getRowVerticalExtent(subs, false, this.subscriptAxis);
        var subHeight = rve[0]; var subDepth = rve[1]; var subAscender = rve[2]; var subDescender = rve[3];

        rve = this.getRowVerticalExtent(supers, false, this.superscriptAxis);
        var superHeight = rve[0]; var superDepth = rve[1]; var superAscender = rve[2]; var superDescender = rve[3];

        this.subShift = 0;
        var shiftAttr;
        if(subs.length > 0) {
            shiftAttr = this.getProperty('subscriptshift');
            if(!shiftAttr) shiftAttr = '0.5ex';
            this.subShift = Math.max(this.parseLength(shiftAttr), (subHeight - scriptMedian + gap));
            if(this.alignToAxis) this.subShift += this.axis();
            this.subShift = Math.max(this.subShift, (this.base.depth + protrusion - subDepth));
            this.height = Math.max(this.height, subHeight - this.subShift);
            this.depth = Math.max(this.depth, superDepth + this.subShift);
            this.ascender = Math.max(this.ascender, subAscender - this.subShift);
            this.descender = Math.max(this.descender, subDescender + this.subShift);
        }

        this.superShift = 0;
        if(supers.length > 0) {
            shiftAttr = this.getProperty('superscriptshift');
            if(!shiftAttr) shiftAttr = '1ex';
            this.superShift = Math.max(this.parseLength(shiftAttr), (superDepth + scriptMedian + gap));
            if(this.alignToAxis) this.superShift -= this.axis();
            this.superShift = Math.max(this.superShift, this.base.height + protrusion - superHeight);
            this.height = Math.max(this.height, superHeight + this.superShift);
            this.depth = Math.max(this.depth, superDepth - this.superShift);
            this.ascender = Math.max(this.ascender, superHeight + this.superShift);
            this.descender = Math.max(this.descender, superDepth - this.superShift);
        }

        var parallelWidths = function(nodes1, nodes2) {
            var widths = [];
            var w;
            var maxLen = Math.max(nodes1.length, nodes2.length);
            for(var i = 0; i < maxLen; i++) {
                w = 0;
                if(i < nodes1.length) w = Math.max(w, nodes1[i].width);
                if(i < nodes2.length) w = Math.max(w, nodes2[i].width);
                widths.push(w);
            }
            return widths;
        };

        this.postwidths = parallelWidths(this.subscripts, this.superscripts);
        this.prewidths = parallelWidths(this.presubscripts, this.presuperscripts);
        this.width += this.postwidths.reduce(function(sum, w) { return sum + w; }, 0);
        this.width += this.prewidths.reduce(function(sum, w) { return sum + w; }, 0);
    };

    MathNode.prototype.measureLimits = function(underscript, overscript) {
        if(this.children[0].core.moveLimits) {
            var subs = [];
            var supers = [];
            if(underscript) subs.push(underscript);
            if(overscript) supers.push(overscript);
            this.measureScripts(subs, supers);
            return;
        }

        this.underscript = underscript;
        this.overscript = overscript;
        this.setNodeBase(this.children[0]);

        this.width = this.base.width;
        if(this.overscript) this.width = Math.max(this.width, this.overscript.width);
        if(this.underscript) this.width = Math.max(this.width, this.underscript.width);
        this.base.stretch(this.width);
        if(this.overscript) this.overscript.stretch(this.width);
        if(this.underscript) this.underscript.stretch(this.width);

        gap = this.nominalLineGap();

        if(this.overscript) {
            var overscriptBaselineHeight = this.base.height + gap + this.overscript.depth;
            this.height = overscriptBaselineHeight + this.overscript.height;
            this.ascender = this.height;
        }
        else {
            this.height = this.base.height;
            this.ascender = this.base.ascender;
        }

        if(this.underscript) {
            var underscriptBaselineDepth = this.base.depth + gap + this.underscript.height;
            this.depth = underscriptBaselineDepth + this.underscript.depth;
            this.descender = this.depth;
        }
        else {
            this.depth = this.base.depth;
            this.descender = this.base.descender;
        }
    };

    MathNode.prototype.stretch = function(toWidth, toHeight, toDepth, symmetric) {
        if(!this.core.stretchy) return;

        if(this != this.base) {
            if(toWidth !== null && toWidth !== undefined) toWidth -= (this.width - this.base.width);
            this.base.stretch(toWidth, toHeight, toDepth, symmetric);
            this.measureNode();
        }
        else if(this.element.tagName == 'mo') {
            if(this.fontSize === 0) return;

            var maxsizedefault = this.opdefaults['maxsize'];
            var maxsizeattr = this.getProperty('maxsize', maxsizedefault);
            var maxScale = null;
            if(maxsizeattr != 'infinity') maxScale = this.parseSpaceOrPercent(maxsizeattr, this.fontSize, this.fontSize) / this.fontSize;

            var minsizedefault = this.opdefaults['minsize'];
            var minsizeattr = this.getProperty('minsize', minsizedefault);
            var minScale = this.parseSpaceOrPercent(minsizeattr, this.fontSize, this.fontSize) / this.fontSize;

            if(toWidth === null || toWidth === undefined) {
                this.stretchVertically(toHeight, toDepth, minScale, maxScale, symmetric);
            }
            else {
                this.stretchHorizontally(toWidth, minScale, maxScale);
            }
        }
    };

    MathNode.prototype.stretchVertically = function(toHeight, toDepth, minScale, maxScale, symmetric) {
        if((this.ascender + this.descender) === 0) return;
        if(this.scaling == 'horizontal') return;

        if(symmetric && this.symmetric) {
            toHeight = (toHeight + toDepth) / 2.0;
            toDepth = toHeight;
        }

        var scale = (toHeight + toDepth) / (this.ascender + this.descender);

        if(minScale) scale = Math.max(scale, minScale);
        if(maxScale) scale = Math.min(scale, maxScale);

        this.fontSize *= scale;
        this.height *= scale;
        this.depth *= scale;
        this.ascender *= scale;
        this.descender *= scale;
        this.textShift *= scale;

        var extraShift = ((toHeight - this.ascender) -(toDepth - this.descender)) / 2.0;
        this.textShift += extraShift;
        this.height += extraShift;
        this.ascender += extraShift;
        this.depth -= extraShift;
        this.descender -= extraShift;

        if(this.scaling == 'vertical') {
            this.textStretch /= scale;
        }
        else {
            this.width *= scale;
            this.leftBearing *= scale;
            this.rightBearing *= scale;
        }
    };

    MathNode.prototype.stretchHorizontally = function(toWidth, minScale, maxScale) {
        if(this.width === 0) return;
        if(this.scaling != 'horizontal') return;

        var scale = toWidth / this.width;
        if(minScale) scale = Math.max(scale, minScale);
        if(maxScale) scale = Math.min(scale, maxScale);

        this.width *= scale;
        this.textStretch *= scale;
        this.leftBearing *= scale;
        this.rightBearing *= scale;
    };

    MathNode.prototype.setNodeBase = function(base) {
        this.base = base;
        this.core = base.core;
        this.alignToAxis = base.alignToAxis;
        this.stretchy = base.stretchy;
    };

    MathNode.prototype.wrapChildren = function(wrapperElement) {
        var old_children = this.children;
        this.children = [];
        var base = this.createNode(wrapperElement, {});
        base.children = old_children;
    };

    MathNode.prototype.createImplicitRow = function() {
        if(this.children.length != 1) {
            this.wrapChildren('mrow');
            this.children[0].makeContext();
            this.children[0].measureNode();
        }
        this.setNodeBase(this.children[0]);
    };

    MathNode.prototype.getVerticalStretchExtent = function(descendants, rowAlignToAxis, axis) {
        var ascender = 0;
        var descender = 0;
        for(var i = 0; i < descendants.length; i++) {
            var ch = descendants[i];
            var asc;
            var desc;
            if(ch.core.stretchy) {
                asc = ch.core.ascender;
                desc = ch.core.descender;
            }
            else {
                asc = ch.ascender;
                desc = ch.descender;
            }
            if(ch.alignToAxis && !rowAlignToAxis) {
                asc += axis;
                desc -= axis;
            }
            else if(!ch.alignToAxis && rowAlignToAxis) {
                var chaxis = ch.axis();
                asc -= chaxis;
                desc += chaxis;
            }
            ascender = Math.max(asc, ascender);
            descender = Math.max(desc, descender);
        }
        return [ascender, descender];
    };

    MathNode.prototype.getRowVerticalExtent = function(descendants, rowAlignToAxis, axis) {
        var height = 0;
        var depth = 0;
        var ascender = 0;
        var descender = 0;
        for(var i = 0; i < descendants.length; i++) {
            var ch = descendants[i];
            var h = ch.height;
            var d = ch.depth;
            var asc = ch.ascender;
            var desc = ch.descender;
            if(ch.alignToAxis && !rowAlignToAxis) {
                h += axis;
                asc += axis;
                d -= axis;
                desc -= axis;
            }
            else if(!ch.alignToAxis && rowAlignToAxis) {
                var chaxis = ch.axis();
                h -= chaxis;
                asc -= chaxis;
                d += chaxis;
                desc += chaxis;
            }
            height = Math.max(h, height);
            depth = Math.max(d, depth);
            ascender = Math.max(asc, ascender);
            descender = Math.max(desc, descender);
        }
        return [height, depth, ascender, descender];
    };

    MathNode.prototype.arrangeCells = function() {
        this.rows = [];
        this.columns = [];
        var busycells = [];

        var table_rowaligns = this.getListProperty('rowalign');
        var table_columnaligns = this.getListProperty('columnalign');

        var max_map = function(n) { return Math.max(0, n - 1); };
        var max_reduce = function(max, n) { return Math.max(max, n); };
        var rowalign;

        for(var i = 0; i < this.children.length; i++) {
            var ch = this.children[i];
            rowalign = getByIndexOrLast(table_rowaligns, this.rows.length);
            var row_columnaligns = table_columnaligns;
            var cells;
            if(ch.element.tagName == 'mtr' || ch.element.tagName == 'mlabeledtr') {
                cells = ch.children;
                rowalign = ch.getAttribute('rowalign', rowalign);
                if(ch.hasAttribute('columnalign')) {
                    row_columnaligns = ch.getListProperty('columnalign');
                }
            }
            else {
                cells = [ch];
            }

            row = createRowDescriptor(this, cells, rowalign, row_columnaligns, busycells);
            this.rows.push(row);
            busycells = busycells.map(max_map);
            while(busycells.length < row.cells.length) busycells.push(0);

            for(var j = 0; j < row.cells.length; j ++) {
                var cell = row.cells[j];
                if(cell === undefined || cell === null) continue;
                if(cell.rowspan > 1) {
                    for(var k = j; k < j + cell.colspan; k++) {
                        busycells[k] = cell.rowspan - 1;
                    }
                }
            }
        }

        while(busycells.reduce(max_reduce, 0) > 0) {
            rowalign = getByIndexOrLast(table_rowaligns, this.rows.length);
            this.rows.push(createRowDescriptor(this, [], rowalign, table_columnaligns, busycells));
            busycells = busycells.map(max_map);
        }
    };

    MathNode.prototype.arrangeLines = function() {
        var tmpspacings = this.getListProperty('rowspacing');
        var spacings = [];
        var i;
        for(i = 0; i < tmpspacings.length; i++) {
            spacings.push(this.parseSpace(tmpspacings[i]));
        }
        var lines = this.getListProperty('rowlines');
        var line;
        for(i = 0; i < this.rows.length - 1; i++) {
            this.rows[i].spaceAfter = getByIndexOrLast(spacings, i);
            line = getByIndexOrLast(lines, i);
            if(line != 'none') {
                this.rows[i].lineAfter = line;
                this.rows[i].spaceAfter += this.lineWidth;
            }
        }

        tmpspacings = this.getListProperty('columnspacing');
        spacings = [];
        for(i = 0; i < tmpspacings.length; i++) {
            spacings.push(this.parseSpace(tmpspacings[i]));
        }
        lines = this.getListProperty('columnlines');

        for(i = 0; i < this.columns.length - 1; i++) {
            this.columns[i].spaceAfter = getByIndexOrLast(spacings, i);
            line = getByIndexOrLast(lines, i);
            if(line != 'none') {
                this.columns[i].lineAfter = line;
                this.columns[i].spaceAfter += this.lineWidth;
            }
        }

        this.framespacings = [0, 0];
        this.framelines = [null, null];

        tmpspacings = this.getListProperty('framespacing');
        spacings = [];
        for(i = 0; i < tmpspacings.length; i++) {
            spacings.push(this.parseSpace(tmpspacings[i]));
        }
        lines = this.getListProperty('frame');
        for(i = 0; i < 2; i++) {
            line = getByIndexOrLast(lines, i);
            if(line != 'none') {
                this.framespacings[i] = getByIndexOrLast(spacings, i);
                this.framelines[i] = getByIndexOrLast(lines, i);
            }
        }
    };

    MathNode.prototype.calculateColumnWidths = function() {
        var fullwidthattr = this.getAttribute('width', 'auto');
        var fullwidth;
        if(fullwidthattr == 'auto') fullwidth = null;
        else {
            fullwidth = this.parseLength(fullwidthattr);
            if(fullwidth <= 0) fullwidth = null;
        }

        var columnwidths = this.getListProperty('columnwidth');
        var i; var j; var r; var c; var column;
        for(i = 0; i < this.columns.length; i++) {
            column = this.columns[i];
            var attr = getByIndexOrLast(columnwidths, i);
            if(['auto', 'fit'].indexOf(attr) >= 0) {
                column.fit = (attr == 'fit');
            }
            else if(attr.match(/%$/)) {
                if(fullwidth === null) {
                    this.error("Percents in column widths supported only in tables with explicit width; width of column " + (i+1) + " treated as 'auto'");
                }
                else {
                    var value = this.parseFloat(attr.substr(0, attr.length-1));
                    if(value && value > 0) {
                        column.width = fullwidth * value / 100;
                        column.auto = false;
                    }
                }
            }
            else {
                column.width = this.parseSpace(attr);
                column.auto = false;
            }
        }

        for(i = 0; i < this.rows.length; i++) {
            r = this.rows[i];
            for(j = 0; j < r.cells.length; j++) {
                c = r.cells[j];
                if((c === null || c === undefined) || (c.content === null || c.content === undefined) || c.colspan > 1) continue;
                column = this.columns[j];
                if(column.auto) column.width = Math.max(column.width, c.content.width);
            }
        }

        var autocolumns_filter = function(x) { return x.auto; };
        var fixedcolumns_filter = function(x) { return !x.auto; };
        var space_after_sum = function(sum, x) { return sum + x; };
        var width_sum = function(sum, x) { return sum + x; };

        while(true) {
            var adjustedColumns = [];
            var adjustedWidth = 0;
            for(i = 0; i < this.rows.length; i++) {
                r = this.rows[i];
                for(j = 0; j < r.cells.length; j++) {
                    c = r.cells[j];
                    if((c === null || c === undefined) || (c.content === null || c.content === undefined) || c.colspan == 1) continue;

                    var columns = this.columns.slice(j, j + c.colspan);
                    var autocolumns = columns.filter(autocolumns_filter);
                    if(autocolumns.length === 0) continue;
                    var fixedcolumns = columns.filter(fixedcolumns_filter);
                    var fixedWidth = 0;
                    if(fixedcolumns.length > 0) {
                        fixedWidth += fixedcolumns.slice(0, fixedcolumns.length-1).reduce(space_after_sum, 0);
                        fixedWidth += fixedcolumns.reduce(width_sum, 0);
                    }
                    var autoWidth = autocolumns.reduce(width_sum, 0);
                    if(c.content.width < fixedWidth + autoWidth) continue;

                    var requiredWidth = c.content.width - fixedWidth;
                    var unitWidth = requiredWidth / autocolumns.length;

                    while(true) {
                        var oversizedColumns = autocolumns.filter(function(x) { return x.width >= unitWidth; });
                        if(oversizedColumns.length === 0) break;

                        autocolumns = autocolumns.filter(function(x) { return x.width < unitWidth; });
                        if(autocolumns.length === 0) break;

                        requiredWidth -= oversizedColumns.reduce(width_sum, 0);
                        unitWidth = requiredWidth / autocolumns.length;
                    }
                    if(autocolumns.length === 0) continue;

                    if(unitWidth > adjustedWidth) {
                        adjustedWidth = unitWidth;
                        adjustedColumns = autocolumns;
                    }
                }
            }

            if(adjustedColumns.length === 0) break;
            for(i = 0; i < adjustedColumns.length; i++) {
                adjustedColumns[i].width = adjustedWidth;
            }
        }

        if(this.getProperty('equalcolumns') == 'true') {
            var acolumns = this.columns.filter(autocolumns_filter);
            var globalWidth = acolumns.reduce(function(max, x) { return Math.max(max, x); }, 0);
            for(i = 0; i < acolumns.length; i++) {
                acolumns[i].width = globalWidth;
            }
        }

        if(fullwidth !== null && fullwidth !== undefined) {
            var delta = fullwidth;
            delta -= this.columns.reduce(width_sum, 0);
            delta -= this.columns.slice(0, this.columns.length-1).reduce(space_after_sum, 0);
            delta -= 2 * this.framespacings[0];
            if(delta !== 0) {
                var sizableColumns = this.columns.filter(function(x) { return x.fit; });
                if(sizableColumns.length === 0) sizableColumns = this.columns.filter(autocolumns_filter);
                if(sizableColumns.length === 0) this.error("Overconstrained table layout: explicit table width specified, but no column has automatic width; table width attribute ignored");
                else {
                    delta /= sizableColumns.length;
                    for(i = 0; i < sizableColumns.length; i++) sizableColumns[i].width += delta;
                }
            }
        }
    };

    MathNode.prototype.calculateRowHeights = function() {
        var commonAxis = this.axis();
        var i; var j; var r; var c;
        for(i = 0; i < this.rows.length; i++) {
            r = this.rows[i];
            r.height = 0;
            r.depth = 0;
            for(j = 0; j < r.cells.length; j++) {
                c = r.cells[j];
                if((c === null || c === undefined) || (c.content === null || c.content === undefined) || c.rowspan != 1) continue;
                var cellAxis = c.content.axis();
                c.vshift = 0;

                if(c.valign == 'baseline') {
                    if(r.alignToAxis) c.vshift -= commonAxis;
                    if(c.content.alignToAxis) c.vshift += cellAxis;
                }
                else if(c.valign == 'axis') {
                    if(!r.alignToAxis) c.vshift += commonAxis;
                    if(!c.content.alignToAxis) c.vshift -= cellAxis;
                }
                else {
                    c.vshift = (r.height - r.depth - c.content.height + c.content.depth) / 2;
                }

                r.height = Math.max(r.height, c.content.height + c.vshift);
                r.depth = Math.max(r.depth, c.content.depth - c.vshift);

            }
        }

        var space_after_sum = function(sum, x) { return sum + x.spaceAfter; };
        var full_height_sum = function(sum, x) { return sum + x.height + x.depth; };

        while(true) {
            var adjustedRows = [];
            var adjustedSize = 0;
            for(i = 0; i < this.rows.length; i++) {
                r = this.rows[i];
                for(j = 0; j < r.cells.length; j++) {
                    c = r.cells[j];
                    if((c === null || c === undefined) || (c.content === null || c.content === undefined) || c.rowspan == 1) continue;
                    var rows = this.rows.slice(i, i + c.rowspan);
                    var requiredSize = c.content.height + c.content.depth;
                    requiredSize -= rows.slice(0, rows.length - 1).reduce(space_after_sum, 0);
                    var fullSize = rows.reduce(full_height_sum, 0);
                    if(fullSize > requiredSize) continue;

                    var unitSize = requiredSize / rows.length;
                    while(true) {
                        var oversizedRows = rows.filter(function(x) { return (x.height + x.depth) >= unitSize; });
                        if(oversizedRows.length === 0) break;

                        rows = rows.filter(function(x) { return (x.height + x.depth) < unitSize; });
                        if(rows.length === 0) break;

                        requiredSize -= oversizedRows.reduce(full_height_sum, 0);
                        unitSize = requiredSize / rows.length;
                    }
                    if(rows.length === 0) continue;

                    if(unitSize > adjustedSize) {
                        adjustedSize = unitSize;
                        adjustedRows = rows;
                    }
                }
            }

            if(adjustedRows.length === 0) break;

            for(i = 0; i < adjustedRows.length; i++) {
                r = adjustedRows[i];
                var delta = (adjustedSize - r.height - r.depth) / 2;
                r.height += delta;
                r.depth += delta;
            }
        }

        if(this.getProperty('equalsrows') == 'true') {
            var maxvsize = this.rows.reduce(function(max, x) { return Math.max(max, x.height + x.depth); }, 0);
            for(i = 0; i < this.rows.length; i++) {
                r = this.rows[i];
                var d = (maxvsize - r.height - r.depth) / 2;
                r.height += d;
                r.depth += d;
            }
        }
    };

    MathNode.prototype.getAlign = function()  {
        var alignattr = this.getProperty('align').trim();
        if(alignattr.length === 0) alignattr = globalDefaults['align'];

        var splitalign = alignattr.split(' ');
        var alignType = splitalign[0];

        var alignRow = null;
        if(splitalign.length != 1) {
            alignRow = this.parseInt(splitalign[1]);
            if(alignRow === 0) {
                this.error("Alignment row number cannot be zero");
                alignRow = null;
            }
            else if(alignRow > this.rows.length) {
                this.error("Alignment row number cannot exceed row count");
                alignRow = this.rows.length;
            }
            else if(alignRow < -this.rows.length) {
                this.error("Negative alignment row number cannot exceed row count");
                alignRow = 1;
            }
            else if(alignRow < 0) {
                alignRow = this.rows.length - alignRow + 1;
            }
        }

        return [alignType, alignRow];
    };

    MathNode.prototype.createSVG = function() {
        var baseline = 0;
        if(this.alignToAxis) baseline = this.axis();

        var height = Math.max(this.height, this.ascender);
        var depth = Math.max(this.depth, this.descender);
        var vsize = height + depth;

        var attrs = {
            'width': this.width,
            'height': vsize,
            'viewBox': '0 ' + (-(height + baseline)) + ' ' + this.width + ' ' + vsize
        };

        var doc = createDocument(SVG_NS, 'svg', null);
        var svg = doc.documentElement;
        for(var attr in attrs) {
            svg.setAttribute(attr, attrs[attr]);
        }

        var metadata = createSVGNode(doc, 'metadata', {});
        svg.appendChild(metadata);

        var metricsattrs = {
            'baseline': (depth - baseline),
            'axis': (depth - baseline + this.axis()),
            'top': (depth + this.height),
            'bottom': (depth - this.depth)
        };

        var metrics = createSVGMATHNode(doc, 'metrics', metricsattrs);
        metadata.appendChild(metrics);

        this.drawTranslatedNodeSVG(svg, 0, -baseline);

        return doc;
    };

    MathNode.prototype.draw_svg = function(parent) {
        var method = this['draw_svg_' + this.element.tagName];
        if(method) method.call(this, parent);
        else this.default_svg_draw(parent);
    };

    MathNode.prototype.default_svg_draw = function(parent) {};

    MathNode.prototype.draw_svg_math = function(parent) {
        this.draw_svg_mrow(parent);
    };

    MathNode.prototype.draw_svg_mrow = function(parent) {
        this.drawBoxSVG(parent);
        if(this.children.length === 0) return;

        var offset = -this.children[0].leftspace;
        for(var i = 0; i < this.children.length; i++) {
            var ch = this.children[i];
            offset += ch.leftspace;
            var baseline = 0;
            if(ch.alignToAxis && !this.alignToAxis) {
                baseline = -this.axis();
            }
            ch.drawTranslatedNodeSVG(parent, offset, baseline);
            offset += ch.width + ch.rightspace;
        }
    };

    MathNode.prototype.draw_svg_mphantom = function(parent) {};

    MathNode.prototype.draw_svg_none = function(parent) {};

    MathNode.prototype.draw_svg_mfrac = function(parent) {
        this.drawBoxSVG(parent);

        var numerator = this.enumerator;
        var denominator = this.denominator;

        if(this.getProperty('bevelled') == 'true') {
            numerator.drawTranslatedNodeSVG(parent, 0, numerator.height - this.height);
            denominator.drawTranslatedNodeSVG(parent, this.width - denominator.width, this.depth - denominator.depth);
        }
        else {
            var numalign = this.getAlignProperty('enumalign');
            var denomalign = this.getAlignProperty('denomalign');

            numerator.drawTranslatedNodeSVG(parent, this.rulewidth + (this.width - 2 * this.rulewidth - numerator.width) * numalign, numerator.height - this.height);
            denominator.drawTranslatedNodeSVG(parent, this.rulewidth + (this.width - 2 * this.rulewidth - denominator.width) * denomalign, this.depth - denominator.depth);
        }

        if(this.rulewidth) {
            var x1, x2, y1, y2;
            if(this.getProperty('bevelled') == 'true') {
                var nh = numerator.height + numerator.depth;
                var dh = denominator.height + denominator.depth;

                var ruleX = (this.width + numerator.width - denominator.width) / 2.0;
                var ruleY;
                if(nh < dh) ruleY = 0.75 * nh - this.height;
                else ruleY = this.depth - 0.75 * dh;

                x1 = Math.max(0, ruleX - (this.depth - ruleY) / this.slope);
                x2 = Math.min(this.width, ruleX + (ruleY + this.height) / this.slope);
                y1 = Math.min(this.depth, ruleY + ruleX * this.slope);
                y2 = Math.max(-this.height, ruleY - (this.width - ruleX) * this.slope);
            }
            else {
                x1 = 0;
                y1 = 0;
                x2 = this.width;
                y2 = 0;
            }

            this.drawLineSVG(parent, this.color, this.rulewidth, x1, y1, x2, y2, { 'stroke-linecap': 'butt' });
        }
    };

    MathNode.prototype.draw_svg_maction = function(parent) {
        if(this.base) this.base.draw_svg(parent);
    };

    MathNode.prototype.draw_svg_mprescripts = function(parent) {};

    MathNode.prototype.draw_svg_mstyle = function(parent) {
        this.draw_svg_mrow(parent);
    };

    MathNode.prototype.draw_svg_mfenced = function(parent) {
        this.draw_svg_mrow(parent);
    };

    MathNode.prototype.draw_svg_merror = function(parent) {
        this.drawBoxSVG(parent, this.borderWidth, 'red');
        this.base.drawTranslatedNodeSVG(parent, this.borderWidth, 0);
    };

    MathNode.prototype.draw_svg_mpadded = function(parent) {
        this.drawBoxSVG(parent);
        this.base.drawTranslatedNodeSVG(parent, this.leftpadding, 0);
    };

    MathNode.prototype.draw_svg_msqrt = function(parent) {
        this.drawBoxSVG(parent);
        this.base.drawTranslatedNodeSVG(parent, this.width - this.base.width - this.gap, 0);

        var x1 = this.width - this.base.width - this.rootWidth - 2 * this.gap;
        var y1 = (this.rootDepth - this.rootHeight) / 2;

        var x2 = x1 + this.rootWidth * 0.2;
        var y2 = y1;

        var x3 = x1 + this.rootWidth * 0.6;
        var y3 = this.rootDepth;

        var x4 = x1 + this.rootWidth;
        var y4 = - this.rootHeight + this.lineWidth / 2;

        var x5 = this.width;
        var y5 = y4;

        var slopeA = (x2 - x3) / (y2 - y3);
        var slopeB = (x3 - x4) / (y3 - y4);

        var x2a = x2 + (this.thickLineWidth - this.lineWidth);
        var y2a = y2;

        var x2c = x2 + this.lineWidth * slopeA / 2;
        var y2c = y2 + this.lineWidth * 0.9;

        var x2b = x2c + (this.thickLineWidth - this.lineWidth) / 2;
        var y2b = y2c;

        var ytmp = y3 - this.lineWidth / 2;
        var xtmp = x3 - this.lineWidth * (slopeA + slopeB) / 4;

        var y3a = (y2a * slopeA - ytmp * slopeB + xtmp - x2a) / (slopeA - slopeB);
        var x3a = xtmp + (y3a - ytmp) * slopeB;

        var y3b = (y2b * slopeA - ytmp * slopeB + xtmp - x2b) / (slopeA - slopeB);
        var x3b = xtmp + (y3b - ytmp) * slopeB;

        y1 += (x2 - x1) * slopeA;

        var attrs = {
            'stroke': this.color,
            'fill': 'none',
            'stroke-width': this.lineWidth,
            'stroke-linecap': 'butt',
            'stroke-linejoin': 'miter',
            'stroke-miterlimit': '10',
            'd': 'M {0} {1} L {2} {3} L {4} {5} L {6} {7} L {8} {9} L {10} {11} L {12} {13} L {14} {15} L {16} {17}'.format(x1, y1, x2a, y2a, x3a, y3a, x3b, y3b, x2b, y2b, x2c, y2c, x3, y3,  x4, y4,  x5, y5)
        };

        var path = createSVGNode(parent.ownerDocument, 'path', attrs);
        parent.appendChild(path);
    };

    MathNode.prototype.draw_svg_mroot = function(parent) {
        this.draw_svg_msqrt(parent);
        if(this.rootindex) {
            var w = Math.max(0, this.cornerWidth - this.rootindex.width) / 2;
            var h = -this.rootindex.depth - this.rootHeight + this.cornerHeight;
            this.rootindex.drawTranslatedNodeSVG(parent, w, h);
        }
    };

    MathNode.prototype.draw_svg_mo = function(parent) {
        this.drawTextSVG(parent);
    };

    MathNode.prototype.draw_svg_mi = function(parent) {
        this.drawTextSVG(parent);
    };

    MathNode.prototype.draw_svg_mn = function(parent) {
        this.drawTextSVG(parent);
    };

    MathNode.prototype.draw_svg_mtext = function(parent) {
        this.drawTextSVG(parent);
    };

    MathNode.prototype.draw_svg_ms = function(parent) {
        this.drawTextSVG(parent);
    };

    MathNode.prototype.draw_svg_mspace = function(parent) {
        this.drawBoxSVG(parent);
    };

    MathNode.prototype.draw_svg_msub = function(parent) {
        this.drawScriptsSVG(parent);
    };

    MathNode.prototype.draw_svg_msup = function(parent) {
        this.drawScriptsSVG(parent);
    };

    MathNode.prototype.draw_svg_msubsup = function(parent) {
        this.drawScriptsSVG(parent);
    };

    MathNode.prototype.draw_svg_mmultiscripts = function(parent) {
        this.drawScriptsSVG(parent);
    };

    MathNode.prototype.drawScriptsSVG = function(parent) {
        if(this.children.length < 2) {
            this.draw_svg_mrow(parent);
            return;
        }

        var subY = this.subShift;
        var superY = -this.superShift;

        var adjustment = function(script) {
            if(script.alignToAxis) return script.axis();
            else return 0;
        };

        this.drawBoxSVG(parent);
        var offset = 0;
        var i;
        for(i = 0; i < this.prewidths.length; i++) {
            offset += this.prewidths[i];
            if(i < this.presubscripts.length) {
                var presubscript = this.presubscripts[i];
                presubscript.drawTranslatedNodeSVG(parent, offset - presubscript.width, subY - adjustment(presubscript));
            }
            if(i < this.presuperscripts.length) {
                var presuperscript = this.presuperscripts[i];
                presuperscript.drawTranslatedNodeSVG(parent, offset - presuperscript.width, superY - adjustment(presuperscript));
            }
        }

        this.base.drawTranslatedNodeSVG(parent, offset, 0);
        offset += this.base.width;

        for(i = 0; i < this.postwidths.length; i++) {
            if(i < this.subscripts.length) {
                var subscript = this.subscripts[i];
                subscript.drawTranslatedNodeSVG(parent, offset, subY - adjustment(subscript));
            }
            if(i < this.superscripts.length) {
                var superscript = this.superscripts[i];
                superscript.drawTranslatedNodeSVG(parent, offset, superY - adjustment(superscript));
            }
            offset += this.postwidths[i];
        }
    };

    MathNode.prototype.draw_svg_munder = function(parent) {
        this.drawLimitsSVG(parent);
    };

    MathNode.prototype.draw_svg_mover = function(parent) {
        this.drawLimitsSVG(parent);
    };

    MathNode.prototype.draw_svg_munderover = function(parent) {
        this.drawLimitsSVG(parent);
    };

    MathNode.prototype.drawLimitsSVG = function(parent) {
        if(this.children.length < 2) {
            this.draw_svg_mrow(parent);
            return;
        }

        if(this.core.moveLimits) {
            this.drawScriptsSVG(parent);
            return;
        }

        this.drawBoxSVG(parent);
        this.base.drawTranslatedNodeSVG(parent, (this.width - this.base.width) / 2, 0);

        if(this.underscript) {
            this.underscript.drawTranslatedNodeSVG(parent, (this.width - this.underscript.width) / 2, this.depth - this.underscript.depth);
        }
        if(this.overscript) {
            this.overscript.drawTranslatedNodeSVG(parent, (this.width - this.overscript.width) / 2, this.overscript.height - this.height);
        }
    };

    MathNode.prototype.draw_svg_menclose = function(parent) {
        if(!this.decoration) {
            this.base.draw(parent);
        }
        else if(this.decoration == 'strikes') {
            this.drawStrikesEnclosureSVG(parent);
        }
        else if(this.decoration == 'borders') {
            this.drawBordersEnclosureSVG(parent);
        }
        else if(this.decoration == 'box') {
            this.drawBoxEnclosureSVG(parent);
        }
        else if(this.decoration == 'roundedbox') {
            var r = (this.width - this.base.width + this.height - this.base.height + this.depth - this.base.height) / 4;
            this.drawBoxEnclosureSVG(parent, r);
        }
        else if(this.decoration == 'circle') {
            this.drawCircleEnclosureSVG(parent);
        }
        else if(this.decoration == 'longdiv') {
            this.drawLongDivisionEnclosureSVG(parent);
        }
        else {
            this.error("Internal error: unhandled decoration " + this.decoration);
            this.base.draw_svg(parent);
        }
    };

    MathNode.prototype.drawBoxEnclosureSVG = function(parent, radius) {
        this.drawBoxSVG(parent, this.borderWidth, null, radius);
        this.base.drawTranslatedNodeSVG(parent, (this.width - this.base.width) / 2, 0);
    };

    MathNode.prototype.drawCircleEnclosureSVG = function(parent) {
        var background = this.getBackground();
        var attrs = {
            'fill': background,
            'stroke': this.color,
            'stroke-width': this.borderWidth,
            'cx': this.cx,
            'cy': this.cy,
            'rx': this.rx,
            'ry': this.ry
        };
        var e = createSVGNode(parent.ownerDocument, 'ellipse', attrs);
        parent.appendChild(e);
        this.base.drawTranslatedNodeSVG(parent, (this.width - this.base.width) / 2, 0);
    };

    MathNode.prototype.drawBordersEnclosureSVG = function(parent) {
        this.drawBoxSVG(parent);

        var x1 = this.borderWidth / 2;
        var y1 = this.borderWidth / 2 - this.height;
        var x2 = this.width - this.borderWidth / 2;
        var y2 = this.depth - this.borderWidth / 2;

        if(this.decorationData[0]) {
            this.drawLineSVG(parent, this.color, this.borderWidth, x1, y1, x1, y2);
        }
        if(this.decorationData[1]) {
            this.drawLineSVG(parent, this.color, this.borderWidth, x2, y1, x2, y2);
        }
        if(this.decorationData[2]) {
            this.drawLineSVG(parent, this.color, this.borderWidth, x1, y1, x2, y1);
        }
        if(this.decorationData[3]) {
            this.drawLineSVG(parent, this.color, this.borderWidth, x1, y2, x2, y2);
        }

        var offset = 0;
        if(this.decorationData[0]) {
            offset = this.width - this.base.width;
            if(this.decorationData[1]) offset /= 2;
        }

        this.base.drawTranslatedNodeSVG(parent, offset, 0);
    };

    MathNode.prototype.drawStrikesEnclosureSVG = function(parent) {
        this.drawBoxSVG(parent);
        this.base.draw_svg(parent);

        var mid_x = this.width / 2;
        var mid_y = (this.depth - this.height) / 2;

        if(this.decorationData[0]) {
            this.drawLineSVG(parent, this.color, this.borderWidth, 0, mid_y, this.width, mid_y);
        }
        if(this.decorationData[1]) {
            this.drawLineSVG(parent, this.color, this.borderWidth, mid_x, -this.height, mid_x, this.depth);
        }
        if(this.decorationData[2]) {
            this.drawLineSVG(parent, this.color, this.borderWidth, 0, this.depth, this.width, -this.height);
        }
        if(this.decorationData[3]) {
            this.drawLineSVG(parent, this.color, this.borderWidth, 0, -this.height, this.width, this.depth);
        }
    };

    MathNode.prototype.drawLongDivisionEnclosureSVG = function(parent) {
        this.drawBoxSVG(parent);
        this.base.drawTranslatedNodeSVG(parent, this.width - this.base.width - this.gap, 0);

        var x1 = this.width - this.base.width - ((this.rootWidth * 3) /4) - 2 * this.gap;
        var y1 = this.depth;

        var x2 = this.width - this.base.width - ((this.rootWidth * 3) /4) - 2 * this.gap;
        var y2 = -this.height;

        var x3 = this.width;
        var y3 = -this.height;

        var q1 = (this.rootWidth * 3) / 4;
        var q2 = (this.depth - this.height) / 2;

        var attrs = {
            'stroke': this.color,
            'fill': 'none',
            'stroke-width': this.lineWidth,
            'stroke-linecap': 'butt',
            'stroke-linejoin': 'miter',
            'stroke-miterlimit': '10',
            'd': 'M {0} {1} Q {2} {3} {4} {5} L {6} {7}'.format(x1, y1, q1, q2, x2, y2, x3, y3)
        };

        var e = createSVGNode(parent.ownerDocument, 'path', attrs);
        parent.appendChild(e);
    };

    MathNode.prototype.draw_svg_mtd = function(parent) {
        this.draw_svg_mrow(parent);
    };

    MathNode.prototype.draw_svg_mtr = function(parent) {};

    MathNode.prototype.draw_svg_mlabeledtr = function(parent) {};

    MathNode.prototype.draw_svg_mtable = function(parent) {
        this.drawBoxSVG(parent);

        var vshift = -this.height + this.framespacings[1];
        var hshift = 0;
        var r; var c; var row; var column; var cell;

        var width_sum = function(sum, x) { return sum + x.width; };
        var height_depth_sum = function(sum, x) { return sum + x.height + x.depth; };
        var space_after_sum = function(sum, x) { return sum + x.spaceAfter; };

        for(r = 0; r < this.rows.length; r++) {
            row = this.rows[r];
            vshift += row.height;
            hshift = this.framespacings[0];
            for(c = 0; c < row.cells.length; c++) {
                column = this.columns[c];
                cell = row.cells[c];
                if(cell && cell.content) {
                    var cellWidth;
                    if(cell.colspan > 1) {
                        cellWidth = this.columns.slice(c, c + cell.colspan).reduce(width_sum, 0);
                        cellWidth += this.columns.slice(c, c + cell.colspan - 1).reduce(space_after_sum, 0);
                    }
                    else {
                        cellWidth = column.width;
                    }

                    var align = alignKeywords[cell.halign];
                    if(align === null) align = 0.5;
                    var hadjust = (cellWidth - cell.content.width) * align;

                    var cellHeight;
                    if(cell.rowspan > 1) {
                        cellHeight = this.rows.slice(r, r + cell.rowspan).reduce(height_depth_sum, 0);
                        cellHeight += this.rows.slice(r, r + cell.rowspan - 1).reduce(space_after_sum, 0);
                    }
                    else {
                        cellHeight = row.height + row.depth;
                    }

                    var vadjust;
                    if(cell.valign == 'top') vadjust = cell.content.height - row.height;
                    else if(cell.valign == 'bottom') vadjust = cellHeight - row.height - cell.content.depth;
                    else if((cell.valign == 'axis' || cell.valign == 'baseline') && cell.rowspan == 1) vadjust = -cell.vshift;
                    else vadjust = (cell.content.height - cell.content.depth + cellHeight) / 2 - row.height;
                    cell.content.drawTranslatedNodeSVG(parent, hshift + hadjust, vshift + vadjust);
                }
                hshift += column.width + column.spaceAfter;
            }
            vshift += row.depth + row.spaceAfter;
        }

        var drawBorder = function(x1, y1, x2, y2, linestyle) {
            if(!linestyle) return;
            if(x1 == x2 && y1 == y2) return;
            var extrastyle;
            if(linestyle == 'dashed') {
                var linelength = Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
                var dashoffset = 5 - ((linelength / this.lineWidth + 3) % 10) / 2;
                extrastyle = {
                    'stroke-dasharray': ((this.lineWidth * 7) + ',' + (this.lineWidth * 3)),
                    'stroke-dashoffset': this.lineWidth * dashoffset
                };
            }
            this.drawLineSVG(parent, this.color, this.lineWidth, x1, y1, x2, y2, extrastyle);
        };

        var x1, x2, y1, y2; var spacing; var j;
        x1 = this.lineWidth / 2;
        y1 = this.lineWidth / 2 - this.height;
        x2 = this.width - this.lineWidth / 2;
        y2 = this.depth - this.lineWidth / 2;

        drawBorder.call(this, x1, y1, x1, y2, this.framelines[0]);
        drawBorder.call(this, x2, y1, x2, y2, this.framelines[0]);
        drawBorder.call(this, x1, y1, x2, y1, this.framelines[1]);
        drawBorder.call(this, x1, y2, x2, y2, this.framelines[1]);

        hshift = this.framespacings[0];
        var hoffsets = [];
        for(c = 0; c < this.columns.length; c++) {
            spacing = this.columns[c].spaceAfter;
            hshift += this.columns[c].width;
            hoffsets.push(hshift + spacing / 2);
            hshift += spacing;
        }
        hoffsets[hoffsets.length - 1] = x2;

        vshift = -this.height + this.framespacings[1];
        var voffsets = [];
        for(r = 0; r < this.rows.length; r++) {
            spacing = this.rows[r].spaceAfter;
            vshift += this.rows[r].height + this.rows[r].depth;
            voffsets.push(vshift + spacing / 2);
            vshift += spacing;
        }
        voffsets[voffsets.length - 1] = y2;

        var n1max = function(x) { return Math.max(0, x-1); };

        var vspans = [];
        for(c = 0; c < this.columns.length; c++) vspans.push(0);
        for(r = 0; r < this.rows.length-1; r++) {
            row = this.rows[r];
            if(row.lineAfter === null || row.lineAfter === undefined) continue;

            for(c = 0; c < row.cells.length; c++) {
                cell = row.cells[c];
                if(!(cell && cell.content)) continue;
                for(j = c; j < c + cell.colspan; j++) {vspans[j] = cell.rowspan;}
            }
            vspans = vspans.map(n1max);

            var lineY = voffsets[r];
            var startX = x1;
            var endX = x1;
            for(c = 0; c < this.columns.length; c++) {
                if(vspans[c] > 0) {
                    drawBorder.call(this, startX, lineY, endX, lineY, row.lineAfter);
                    startX = hoffsets[c];
                }
                endX = hoffsets[c];
            }
            drawBorder.call(this, startX, lineY, endX, lineY, row.lineAfter);
        }

        var hspans = [];
        for(r = 0; r < this.rows.length; r++) hspans.push(0);
        for(c = 0; c < this.columns.length - 1; c++) {
            column = this.columns[c];
            if(column.lineAfter === null || column.lineAfter === undefined) continue;

            for(r = 0; r < this.rows.length; r++) {
                row = this.rows[r];
                if(row.cells.length <= c) continue;
                cell = row.cells[c];
                if(!(cell && cell.content)) continue;
                for(j = r; j < r + cell.rowspan; j++) {
                    hspans[j] = cell.colspan;
                }
            }
            hspans = hspans.map(n1max);

            var lineX = hoffsets[c];
            var startY = y1;
            var endY = y1;
            for(r = 0; r < this.rows.length; r++) {
                if(hspans[r] > 0) {
                    drawBorder.call(this, lineX, startY, lineX, endY, column.lineAfter);
                    startY = voffsets[r];
                }
                endY = voffsets[r];
            }
            drawBorder.call(this, lineX, startY, lineX, endY, column.lineAfter);
        }
    };

    MathNode.prototype.drawTranslatedNodeSVG = function(parent, dx, dy) {
        if(dx === 0 && dy === 0) {
            this.draw_svg(parent);
        }
        else {
            var e = createSVGNode(parent.ownerDocument, 'g', { 'transform': 'translate(' + dx + ', ' + dy + ')' });
            parent.appendChild(e);
            this.draw_svg(e);
        }
    };

    MathNode.prototype.drawBoxSVG = function(parent, borderWidth, borderColor, borderRadius) {
        if(borderWidth === undefined) borderWidth = 0;
        if(borderColor === undefined) borderColor = null;
        if(borderRadius === undefined) borderRadius = 0;
        var background = this.getBackground();
        if(background == 'none' && (borderWidth === null || borderWidth === 0)) return;

        if(borderColor === null) borderColor = this.color;

        var attrs = {
            'fill': background,
            'stroke': 'none',
            'x': (borderWidth / 2),
            'y': (borderWidth / 2 - this.height),
            'width': (this.width - borderWidth),
            'height': (this.height + this.depth - borderWidth)
        };

        if(borderWidth !== 0 && borderColor !== null && borderColor !== undefined) {
            attrs['stroke'] = borderColor;
            attrs['stroke-width'] = borderWidth;
            if(borderRadius !== 0) {
                attrs['rx'] = borderRadius;
                attrs['ry'] = borderRadius;
            }
        }

        var e = createSVGNode(parent.ownerDocument, 'rect', attrs);
        parent.appendChild(e);
    };

    MathNode.prototype.drawLineSVG = function(parent, color, width, x1, y1, x2, y2, strokeattrs) {
        if(strokeattrs === undefined) strokeattrs = null;
        var attrs = {
            'fill': 'none',
            'stroke': color,
            'stroke-width': width,
            'stroke-linecap': 'square',
            'stroke-dasharray': 'none',
            'x1': x1, 'y1': y1,
            'x2': x2, 'y2': y2
        };
        if(strokeattrs !== null) {
            for(var attr in strokeattrs) {
                attrs[attr] = strokeattrs[attr];
            }
        }

        var e = createSVGNode(parent.ownerDocument, 'line', attrs);
        parent.appendChild(e);
    };

    MathNode.prototype.drawTextSVG = function(parent) {
        this.drawBoxSVG(parent);

        var fontfamilies = this.fontpool().filter(function(f) { return f.used; }).map(function(f) { return f.family; });
        if(fontfamilies.length === 0) fontfamilies = this.fontfamilies;

        var attrs = {
            'fill': this.color,
            'font-family': fontfamilies.join(", "),
            'font-size': this.fontSize,
            'text-anchor': 'middle',
            'x': ((this.width + this.leftBearing - this.rightBearing) / 2 / this.textStretch),
            'y': (-this.textShift)
        };
        if(this.fontweight != 'normal') attrs['font-weight'] = this.fontweight;
        if(this.fontstyle != 'normal') attrs['font-style'] = this.fontstyle;
        if(this.textStretch != 1) attrs['transform'] = 'scale(' + this.textStretch + ', 1)';

        for(var sc in specialChars) {
            this.text.replace(sc, specialChars[sc]);
        }

        var e = createSVGNode(parent.ownerDocument, 'text', attrs);
        e.appendChild(e.ownerDocument.createTextNode(this.text));
        parent.appendChild(e);
    };

    MathNode.prototype.getBackground = function() {
        var value = this.getAttribute('mathbackground');
        if(value) {
            if(value == 'transparent') return 'none';
            else return value;
        }

        value = this.getAttribute('background-color');
        if(value) {
            if(value == 'transparent') return 'none';
            else return value;
        }

        value = this.getAttribute('background');
        if(value) {
            if(value == 'transparent') return 'none';
            else return value;
        }

        return 'none';
    };

    MathNode.prototype.getAlignProperty = function(attrName) {
        var attrValue = this.getProperty(attrName, 'center');
        var value = alignKeywords[attrValue];
        if(!value) {
            this.error("Bad value " + attrValue + " for " + attrName);
            return 0.5;
        }
        else return value;
    };

    return {
        'MathNode': MathNode
    };
})();
