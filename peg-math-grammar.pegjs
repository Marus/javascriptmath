
math= children:(subexpression/comma/semicolon/backslash/junk_operators)* { return {type: 'math', children: children }; }

subexpression= e:(highpriority_identifiers / scripts_styles / fenced / ident_operators / comma / semicolon) { return e; }

nfsubexpression= e:(highpriority_identifiers / scripts_styles / regfenced / operators / relationaloperator / lowpriority_operators / identifiers / number / comma / semicolon) { return e; }

scripts_styles= e:(fractions / stackrel / overunders / specialscripts / scripts / specialovers / specialunders / tablelike / styles / phantom / enclosures / roots / function) { return e; }

ident_operators= e:(escaped_operator / escaped_ident / vertbars / operators / relationaloperator / lowpriority_operators / identifiers / number) { return e; }

escaped_operator= space? "\\u" a:hex b:hex c:hex d:hex space? { return { type: 'escapedoperator', value: a+b+c+d }; }

escaped_ident= space? "\\ui" a:hex b:hex c:hex d:hex space? { return { type: 'escapedident', value: a+b+c+d }; }

hex= d:([0-9]/[a-f]/[A-F]) { return d.toUpperCase(); }

extended_atom= a:(highpriority_identifiers / stackrel / overunders / scripts / atom) { return a; }

atom= atom:(highpriority_identifiers / specialovers / specialunders / tablelike / fenced / phantom / enclosures / roots / function / styles / operators / relationaloperator / identifiers / number / lowpriority_operators) { return atom; }

row= lf:lparen children:subexpression* rf:rparen? { return { type: 'fence', children: children, left: lf, right: rf }; }

regfenced= lf:lfence c:subexpression* rf:rfence? { return { type: 'fence', left: lf, right: rf, children: c}; }

barfenced= lf:vertbars c:nfsubexpression* rf:vertbars { return { type: 'fence', left: lf.value, right: rf.value, children: c }; }

fenced= f:(regfenced/barfenced) { return f; }

fractions= num:extended_atom "/" !"/" !"_" den:extended_atom?  { return { type: 'fraction', numerator: num, denominator: den, bevelled: false }; }

styles= space? style:("mathfrak"/"mathcal"/"mathbf"/"mathsf"/"mathtt"/"mathbb"/"bbb"/"bb"/"sf"/"cc"/"tt"/"fr") space? a:atom? space? { return { type: 'style', style: style, atom: a }; }

specialunders= space? u:("underline" / "ul") space? a:atom space? { return { type: 'specialunder', base: a, script: u }; }

specialovers= space? o:("overline"/ "vector" / "lvec" / "ddot" / "hat" / "bar" / "vec" / "dot") space? a:atom space? { return { type: 'specialover', base: a, script: o }; }

specialscripts= 
		base:(specialscriptoperators/specialscriptidentifiers) pre:prescripts post:postscripts? { return { type: 'multiscripts', base: base, pre: pre, post: post }; }
		/ base:(specialscriptoperators/specialscriptidentifiers) carat over:atom underscore under:subexpression? { return { type: 'overunder', movablelimits: false, base: base, over: over, under: under }; }
		/ base:(specialscriptoperators/specialscriptidentifiers) underscore under:atom carat over:subexpression? { return { type: 'overunder', movablelimits: false, base: base, over: over, under: under }; }
		/ base:(specialscriptoperators/specialscriptidentifiers) carat over:subexpression? { return { type: 'over', movablelimits: false, base: base, script: over }; }
		/ base:(specialscriptoperators/specialscriptidentifiers) underscore under:subexpression? { return { type: 'under', movablelimits: false, base: base, script: under }; }

scripts= base:atom pre:prescripts post:postscripts? { return { type: 'multiscripts', base: base, pre: pre, post: post }; }
		 / base:atom carat sup:atom underscore sub:atom? { return { type: 'subsupscript', base: base, sup: sup, sub: sub }; }
		 / base:atom underscore sub:atom carat sup:atom? { return { type: 'subsupscript', base: base, sup: sup, sub: sub }; }
		 / base:atom underscore sub:atom? { return { type: 'subscript', base: base, script: sub}; }
		 / base:atom carat sup:atom? { return { type: 'superscript', base: base, script: sup}; }

basicscripts= base:()

prescripts= caratplus sup:atom? sub:(underscoreplus a:atom? { return a; })? { return { type: 'prescripts', sub: sub, sup: sup }; }
			/ underscoreplus sub:atom? sup:(caratplus a:atom? { return a; })? { return { type: 'prescripts', sub: sub, sup: sup }; }

postscripts= carat sup:atom? sub:(underscore a:atom? { return a; })? { return { type: 'postscripts', sub: sub, sup: sup }; }
			 / underscore sub:atom? sup:(carat a:atom? { return a; })? { return { type: 'postscripts', sub: sub, sup: sup }; }

overunders= base:atom tricarat over:atom triunderscore under:atom? { return { type: 'overunder', base: base, over: over, under: under }; }
			/ base:atom triunderscore under:atom tricarat over:atom? { return { type: 'overunder', base: base, over: over, under: under }; }
			/ base:atom tricarat over:atom? { return { type: 'over', base: base, script: over }; }
			/ base:atom triunderscore under:atom? { return { type: 'under', base: base, script: under }; }

stackrel= space? "stackrel" space? top:atom? bottom:atom? { return { type: 'over', base: bottom, script: top }; }

enclosure_types= "up" { return 'updiagonalstrike'; }
                / "down" { return 'downdiagonalstrike'; }
                / "\\" { return 'downdiagonalstrike'; }
                / "X" { return 'updiagonalstrike downdiagonalstrike' }
                / "O" { return 'circle'; }
                / "left" { return 'left'; }
                / "right" { return 'right'; }
                / "top" { return 'top'; }
                / "bottom" { return 'bottom'; }
                / "box" { return 'box'; }
                / "rbox" { return 'roundedbox'; }
                / "vert" { return 'verticalstrike'; }
                / "horiz" { return 'horizontalstrike'; }

enclosures= space? et:enclosure_types+ "{" space? children:subexpression* space? "}"? space? { return { type: 'enclosure', notation: et.join(" "), children: children }; }

phantom= space? "ph{" space? children:subexpression* space? "}"? space? { return { type: 'phantom', children: children }; }

tablelike= tl:(align / table / matrix / longdiv / autolongdiv) { return tl; }

longdiv= space? "longdiv" space? lparen? divisor:tablecell? comma? dividend:tablecell? comma? res:tablecell? comma? remain:tablecell? steps:(comma s:tablecell { return s; })* rparen? { return { type: 'longdiv', 'dividend': dividend, 'divisor': divisor, 'result': res, 'remainder':remain, 'steps': steps }; }

autolongdiv= space? "autolongdiv" space? lparen? divisor:number? comma? dividend:number? comma? step:integer? rparen? { return { 'type': 'autolongdiv', 'dividend': dividend, 'divisor': divisor, 'step': step }; }

align= "align" lparen rows:(row:alignrow rows:(sep:(comma/semicolon) row:alignrow { return { line: sep.special == 'semicolon', row:row }; })* { rows.unshift({ row:row }); return rows; })? rparen? { return { type: 'align', rows: rows }; }

alignrow= terms:(highpriority_identifiers / scripts_styles / fenced / ident_operators)* { return { type: 'alignrow', terms: terms }; }

table= space? "tbl" n:"n"? space? opts:tableoptions? space? rows:(lparen rows:(tr:tablerow rows:(s:(comma/semicolon) tr:tablerow? { return { line: s.special == 'semicolon', row:tr } })* { rows.unshift({row: tr}); return rows; })? rparen { return rows; })? {  return { type: 'table', rows: rows, options: opts, nolines:(n == "n") }; }

matrix= space? s:"split"? lf:lfence space? rows:(tr:matrixrow rows:(s:(comma/semicolon) tr:matrixrow? { return { line: s.special == 'semicolon', row: tr } })* { rows.unshift({ row: tr }); return rows; }) space? rf:rfence space? { return { type: 'matrix', split: s == 'split', rows: rows, left:lf, right:rf  }; }

matrixrow= ("("/"[") cells:(tc:tablecell cells:(comma tc:tablecell { return tc; })* { cells.unshift(tc); return cells; })? (")"/"]") { return { type: 'tablerow', cells: cells }; }

tableoptions= "[" aligns:(ha:halign comma? { return ha; })* "]"? space? "["? col:spacingunit? comma? row:spacingunit? comma? hframe:spacingunit? comma? vframe:spacingunit? "]"? { return { type:'tableoptions', aligns:aligns, column:col, row:row, horizontalframe:hframe, verticalframe:vframe }; }

tablerow= lparen cells:(tc:tablecell cells:(comma tc:tablecell { return tc; })* { cells.unshift(tc); return cells; })? rparen { return { type: 'tablerow', cells: cells }; }

tablecell= bse:(highpriority_identifiers / scripts_styles / fenced / ident_operators)* { return {type: 'tablecell', children: bse }; }
 
roots= space? ("sqrt" / "\u221A") space? base:atom? { return { type: 'sqrt', base: base }; }
		/ "root" nth:atom? base:atom? { return { type: 'root', base: base, nth: nth }; }

function= f:advfunction { return f; }

advfunction= f:func sup1:(carat a:atom { return a; })? sub:(underscore a:atom { return a; })? sup2:(carat a:atom { return a; })? of:atom? { return { type: 'func', name: f, of: of, sup: sup1 || sup2, sub: sub }; }
        / f:basicfunc sup1:(carat a:atom { return a; })? sub:(underscore a:atom { return a; })? sup2:(carat a:atom { return a; })? p:primes? of:row { return { type: 'func', primes: p, name: f, of: of, sup: sup1 || sup2, sub: sub }; }

operators= o:(arrows / specialscriptoperators1 / operator / specialscriptoperators2) { return o; }

diffidentifiers= space? "d" !" " !"\r" !"\n" !"\t" i:(specialgreek/greek/otheridents/letter) space? { return { type: 'diffident', identifier: i }; }

identifiers= i:(specialtext / otheridents / specialscriptidentifiers / specialgreek / greek / diffidentifiers / letter / string) { return i; }

slash= space? "/" space? { return "/"; }

doubleslash= slash slash+ { return "//"; }

lparen= space? v:("(" / "\\(") space? { return v; }

rparen= space? v:(")" / "\\)") space? { return v; }

lbracket= space? "[" space? { return "["; }

rbracket= space? "]" space? { return "]"; }

comma= space? "," space? { return { type: 'operator', special: "comma", value:"," }; }

semicolon= space? ";" space? { return { type: 'operator', special: 'semicolon', value: ";" }; }

backslash= space? "\\" space? { return { type: 'operator', special: 'backslash', value: "\\" }; }

carat= space? "^" space? { return "^"; }

underscore= space? "_" !"_|" !"|_" space? { return "_"; }

caratplus= carat carat { return "^^"; }

underscoreplus= space? "__" !"|" space? { return "__"; }

tricarat= carat carat carat { return "^^^" ; }

triunderscore= space? "___" !"|" { return "___"; }

func= space? f:("arctan" / "arcsin" / "arccos" / "sinh" / "cosh" / "tanh" / "coth" / "csch" / "sech" / "sin" / "cos" / "tan" / "csc" / "cot" / "sec" / "log" / "det" / "dim" / "mod" / "exp" / "ker" / "hom" / "Pr" / "gcd" / "lcm" / "lub" / "glb" / "ln" / "lg") space? { return f; }

basicfunc= space? f:("f" / "g" / "h") space? { return f; }

lfence= space? f:("langle" / "{||" / "<<" / "{|" / "(:" / "{:" / "[" / "{" / "(") space? { return f; }

rfence= space? f:("rangle" / "||}" / ">>" / "|}" / ":)" / ":}" / "]" / "}" / ")") space? { return f; }

letter= space? l:[a-zA-Z] space? { return { type: 'variable', value: l }; }

specialgreek= space? v:("betaa" / "Deltaa" / "epsia" / "varepsilon" / "Gammaa" / "lambdaa" / "mua" / "nua" / "varphi" / "Pia" / "upsilona") space? { return { type: 'identifier', value: v }; }

greek= space? v:("alpha" / "beta" / "gamma" / "delta" / "epsi" / "epsilon" / "zeta" / "eta" / "theta" / "iota" / "kappa" / "lambda" / "mu" / "nu" / "xi" / "omicron" / "pi" / "rho" / "sigma" / "tau" / "upsilon" / "phi" / "chi" / "psi" / "omega" / "Alpha" / "Beta" / "Gamma" / "Delta" / "Epsilon" / "Zeta" / "Eta" / "Theta" / "Iota" / "Kappa" / "Lambda" / "Mu" / "Nu" / "Xi" / "Omicron" / "Pi" / "Rho" / "Sigma" / "Tau" / "Upsilon" / "Phi" / "Chi" / "Psi" / "Omega" / "\u03C0" / "\u00DF" / "\u2202" / "\u00B5")  space? { return { type: 'identifier', value: v }; }

otheridents= space? v:("angstrom" / "ddagger" / "currency" / "planck" / "permil" / "pound" / "prime" / "Prime" / "aleph" / "ldots" / "middot" / "cdots" / "vdots" / "rial" / "euro" / "cent" / "sdot" / "ohm" / "yen" / "deg" / "bot" / "_|_" / "CC" / "NN" / "QQ" / "RR" / "ZZ" / "oo" / "O/" / "''''" / "'''" / "''" / "'" / "$") space?
    { return { type: 'identifier', value: v }; }

primes= space? v:("''''" / "'''" / "''" / "'" / "prime" / "Prime") space? { return { type: 'identifier', value: v }; }

specialscriptidentifiers= space? v:(("min"!"us" { return "min"; }) / "max" / "Lim") space? { return { type: 'identifier', value: v }; }

specialtext= space? v:("degrees" / "thickspace" / "emsp13" / "emsp14" / "emsp16" / "emsp" / "ensp" / "numsp" / "puncsp" / "thinsp" / "hairsp" / "medsp" / "NoBreak" / "idiosp" / "%") space? { return { type: 'specialtext', value: v }; }

specialscriptoperators= a:(specialscriptoperators1/specialscriptoperators2) { return a; }

specialscriptoperators1= space? v:("Intersect" / "Union" / "prod" / "sum" / "oint" / "lim" / "\u222B" / "\u220F" / "\u2211" / "nnn" / "uuu") space? { return { type: 'operator', value: v }; }

specialscriptoperators2= space? v:("int") space? { return { type: 'operator', value: v }; }

highpriority_identifiers= space? i:("ddots" / "cdots" / "infty" / "...") space? { return { type: 'identifier', value: i }; }

vertbars= space? f:("lfloor" / "rfloor" / "lceiling" / "rceiling" / "|__" / "__|" / "|~" / "~|" / ("||" !"}" !"~" !"__" !"--" !"==" !"->" { return "||"; }) / ("|" !"}" !"~" !"__" !"--" !"==" !"->" { return "|"; })) space? { return { type: 'vertical_bar', value: f }; }

operator= space? o:("\\ " / "quad" / "qquad" / "emptyset" / "triangle" / "plusminus" / "backslash" / "intersect" / "diamond" / "therefore" / "setminus" / "bigwedge" / "forall" / "exists" / "square" / "divide" / "bigcup" / "bigvee" / "bigcap" / "preceq" / "succeq" / "partial" / "models" / "circ10" / "circ1" / "circ2" / "circ3" / "circ4" / "circ5" / "circ6" / "circ7" / "circ8" / "circ9" / "wedge" / "angle" / "comma" / "union" / "times" / "nabla" / "vdash" / "otimes" / "oplus" / "odot" / "circ" / ("cdot" !"s" { return "cdot"; }) / "grad" / "prec" / "succ" / "star" / "not=" / "pos" / "neg" / "vvv" / "cup" / "cap" / "vee" / "top" / "ell" / ("not"!"in" { return "not"; }) / ("del"!"ta" { return "del"; }) / "|--" / "|==" / "com" / "and" / "And" / "Or" / "or" / "nn" / "uu" / "Re" / "Im" / "wp" / ("ng" !"t" { return "ng"; }) / "AA" / "EE" / "TT" / "vv" / "xx" / "o." / "ox" / "o+" / "pm" / ":-" / "-:" / "+-" / "-+" / ":." / "/_" / "**" / "//" / "\\\\" / "\\," / "\\;" / ("!" !"=" !"<" !">" { return "!"; })  / "#" / "&" / "?" / "@") space? { return { type: 'operator', value: o}; }

lowpriority_operators = space? o:("+" / "-" / "*" / "\u00B1" / "\u00F7" / "\u00AC" / "\u00B0" / "." /  "/" / "^" / ("_" !"_|" !"|" { return "_"; }) / ("~" !"|" { return "~"; }) / "`" / (":" !")" !"}" { return ":"; })) space? { return { type: 'operator', value: o }; }

junk_operators = space? o:("]" / "}" / ")" / "/" / rfence) space? { return { type: 'operator', value: o }; }

arrows= a:(basicarrows/alignarrows) { return a; }

basicarrows= space? a:("Leftrightarrow" / "leftrightarrow" / "Rightarrow" / "rightarrow" / "downarrow" / "Leftarrow" / "leftarrow" / "implies" / "uparrow" / "mapsto" / "lrharr" / "nwarr" / "nwArr" / "nearr" / "neArr" / "swarr" / "swArr" / "searr" / "seArr" / "lrarr" / "uarr" / "uArr" / "darr" / "dArr" / "larr" / "rArr" / "lArr" / "hArr" / "iff" / "varr" / "vArr" / "|->" / "<->" / "-->" / "<--" / "<=>" / "==>" / "<==" / "to" / "->" / "<-" / "=>") space? { return { type: 'arrow', value: a }; }

alignarrows= space? a:("rarr"/"rlarr"/"harr"/"rlharr") space? { return { type: 'align_arrow', value: a }; }

relationaloperator= space? o:("subseteq" / "supseteq" / "subset" / "supset" / "propto" / "approx" / "equiv" / "notin" / "cong" / "prop" / "sube" / "supe" / "sub=" / "sup=" / "sub" / "sup" / "!in" / "leq" / "geq" / "lt=" / "gt=" / "-lt" / "-gt" / "nlt" / "ngt" / "!lt" / "!gt" / "-<=" / ">-=" / "-<" / ">-" / "<=" / "-=" / "~=" / "~~" / ">=" / "!=" / ":=" / "in" / "lt" / "gt" / "ne" / "le" / "ge" / "!<" / "!>" / "=" / ("<"!"<" { return "<"; })/(">"!">" { return ">"; }) / "\u2265" / "\u2264" / "\u2260") space? { return { type: 'relationaloperator', value: o }; }

spacingunit= n:(float / integer) u:unit? { return { type: 'size', size: n, unit: u }; }

halign= space? a:((l:"l" "eft"? {return "left";}) / (r:"r" "ight"? { return "right"; }) / (c:"c" "enter"? {return "center";})) space? { return a; }

unit= space? u:("em" / "ex" / "px" / "pt") space? { return u; }

string= space? "\"" value:[^\"]* "\""? space? { return { type: 'string', value: value.join("") }; }

number= space? v:(float/integer) space? { return { type: 'number', value: v }; }

float= w:[0-9]+ "." f:[0-9]+ { return w.join("") + "." + f.join(""); }

integer= i:[0-9]+ { return i.join(""); }

space= ("ApplyFunction" / " " / "\n" / "\t" / "\r")+ { return { type: 'junkspace' }; }
