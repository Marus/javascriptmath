var mathwalker = {};
with(mathwalker) {
    mathwalker.cal_lookup = [0xEF35,0x212C,0xEF36,0xEF37,0x2130,0x2131,0xEF38,0x210B,0x2110,0xEF39,0xEF3A,0x2112,0x2133,0xEF3B,0xEF3C,0xEF3D,0xEF3E,0x211B,0xEF3F,0xEF40,0xEF41,0xEF42,0xEF43,0xEF44,0xEF45,0xEF46];
    mathwalker.frk_lookup = [0xEF5D,0xEF5E,0x212D,0xEF5F,0xEF60,0xEF61,0xEF62,0x210C,0x2111,0xEF63,0xEF64,0xEF65,0xEF66,0xEF67,0xEF68,0xEF69,0xEF6A,0x211C,0xEF6B,0xEF6C,0xEF6D,0xEF6E,0xEF6F,0xEF70,0xEF71,0x2128];
    mathwalker.ds_lookup = [0xEF8C,0xEF8D,0x2102,0xEF8E,0xEF8F,0xEF90,0xEF91,0x210D,0xEF92,0xEF93,0xEF94,0xEF95,0xEF96,0x2115,0xEF97,0x2119,0x211A,0x211D,0xEF98,0xEF99,0xEF9A,0xEF9B,0xEF9C,0xEF9D,0xEF9E,0x2124];
    
    mathwalker.operator_table = {
        'setminus': { output: '\\' },
        'diamond': { output: '\u22C4' },
        'square': { output: '\u25A1' },
        'prop': { output: '\u221D' },
        'propto': { output: '\u221D' },
        'sube': { output: '\u2286' },
        'subseteq': { output: '\u2286' },
        'supe': { output: '\u2287' },
        'supseteq': { output: '\u2287' },
        'sub=': { output: '\u2286' },
        'sup=': { output: '\u2287' },
        'cdot': { output: '\u22C5' },
        'prod': { output: '\u220F' },
        'sub': { output: '\u2282', attributes: { lspace: 'thickmathspace', rspace: 'thickmathspace', form: 'infix' } },
        'subset': { output: '\u2282', attributes: { lspace: 'thickmathspace', rspace: 'thickmathspace', form: 'infix' } },
        'sup': { output: '\u2283', attributes: { lspace: 'thickmathspace', rspace: 'thickmathspace', form: 'infix' } },
        'supset': { output: '\u2283', attributes: { lspace: 'thickmathspace', rspace: 'thickmathspace', form: 'infix' } },
        '!in': { output: '\u2209' },
        'not': { output: '\u00AC', attributes: { lspace: '0em', rspace: 'thickmathspace', form: 'prefix' } },
        'neg': { output: '\u00AC', attributes: { lspace: '0em', rspace: 'thickmathspace', form: 'prefix' } },
        'sum': { output: '\u2211' },
        'int': { output: '\u222B' },
        'oint': { output: '\u222E' },
        '...': { output: '\u2026' },
        '-<=': { output: '\u2AAF' },
        'preceq': { output: '\u2AAF' },
        '>-=': { output: '\u2AB0' },
        'succeq': { output: '\u2AB0' },
        '-<': { output: '\u227A' },
        'prec': { output: '\u227A' },
        '>-': { output: '\u227B' },
        'succ': { output: '\u227B' },
        '|--': { output: '\u22A2' },
        'vdash': { output: '\u22A2' },
        '|==': { output: '\u22A8' },
        'models': { output: '\u22A8' },
        '\\\\': { output: '\\' },
        'backslash': { output: '\\' },
        'AA': { output: '\u2200' },
        'forall': { output: '\u2200' },
        'EE': { output: '\u2203' },
        'exists': { output: '\u2203' },
        'TT': { output: '\u22A4' },
        'top': { output: '\u22A4' },
        'xx': { output: '\u00D7' },
        'times': { output: '\u00D7' },
        'oo': { output: '\u221E' },
        'o.': { output: '\u2299' },
        'odot': { output: '\u2299' },
        'ox': { output: '\u2297' },
        'otimes': { output: '\u2297' },
        'o+': { output: '\u2295' },
        'oplus': { output: '\u2295' },
        'in': { output: '\u2208' },
        'notin': { output: '\u2209' },
        '~~': { output: '\u2248' },
        '//': { output: '\u2215', attributes: { stretchy: 'false' } },
        '+-': { output: '\u00B1' },
        'pm': { output: '\u00B1' },
        '-+': { output: '\u2213' },
        '-:': { output: '\u00F7' },
        '.=': { output: '\u2250' },
        'divide': { output: '\u00F7' },
        '**': { output: '\u22C6' },
        'star': { output: '\u22C6' },
        ':-': { output: '\u00F7' },
        '-=': { output: '\u2261' },
        'equiv': { output: '\u2261' },
        '~=': { output: '\u2245' },
        'cong': { output: '\u2245' },
        'approx': { output: '\u2248' },
        '!=': { output: '\u2260' },
        'ne': { output: '\u2260' },
        '<=': { output: '\u2264' },
        'le': { output: '\u2264' },
        '>=': { output: '\u2265' },
        'ge': { output: '\u2265' },
        ':=': { output: '\u2254' },
        ':.': { output: '\u2234' },
        'therefore': { output: '\u2234' },
        '.:': { output: '\u2235' },
        '::': { output: '\u2237' },
        '/_': { output: '\u2220', attributes: { lspace: '0px', rspace: '0px' } },
        'angle': { output: '\u2220', attributes: { lspace: '0px', rspace: '0px' } },
        'ell': { output: '\u2113' },
        'triangle': { output: '\u25B3' },
        'Re': { output: '\u211C' },
        'Im': { output: '\u2111' },
        'wp': { output: '\u2118' },
        'emptyset': { output: '\u2205' },
        '*': { output: '\u22C5' },
        '(:': { output: '\u27E8' },
        'langle': { output: '\u27E8' },
        '<<': { output: '\u27E8' },
        ':)': { output: '\u27E9'},
        'rangle': { output: '\u27E9' },
        '>>': { output: '\u27E9' },
        '{:': { output: '' },
        ':}': { output: '' },
        '{|': { output: '|' },
        '{||': { output: '||' },
        '|}': { output: '|' },
        '||}': { output: '||' },
        '||': { output: '\u2016', attributes: { stretchy: 'true' } },
        '-': { output: '\u2212' },
        '@': { output: '\u2218' },
        'circ': { output: '\u2218' },
        '\\ ': { output: '\u00A0' },
        'quad': { output: '\u00A0\u00A0' },
        'qquad': { output: '\u00A0\u00A0\u00A0\u00A0' },
        'uu': { output: '\u222A', attributes: { lspace: 'thinmathspace', rspace: 'thinmathspace', form: 'infix' } },
        'cup': { output: '\u222A', attributes: { lspace: 'thinmathspace', rspace: 'thinmathspace', form: 'infix' } },
        'uuu': { output: '\u22C3' },
        'bigcup': { output: '\u22C3' },
        'nn': { output: '\u2229', attributes: { lspace: 'thinmathspace', rspace: 'thinmathspace', form: 'infix' } },
        'cap': { output: '\u2229', attributes: { lspace: 'thinmathspace', rspace: 'thinmathspace', form: 'infix' } },
        'nnn': { output: '\u22C2' },
        'bigcap': { output: '\u22C2' },
        'Intersect': { output: '\u22C2' },
        'intersect': { output: '\u2229', attributes: { lspace: 'thinmathspace', rspace: 'thinmathspace', form: 'infix' } },
        'Union': { output: '\u22C3' },
        'union': { output: '\u222A', attributes: { lspace: 'thinmathspace', rspace: 'thinmathspace', form: 'infix' } },
        'ng': { output: '\u2212', attributes: { lspace: '1px', rspace: '1px' } },
        'not=': { output: '\u2260' },
        'lt': { output: '<' },
        'gt': { output: '>' },
        'leq': { output: '\u2264' },
        'geq': { output: '\u2265'},
        'lt=': { output: '\u2264' },
        'gt=': { output: '\u2265' },
        '-lt': { output: '\u227A' },
        '-gt': { output: '\u227B' },
        'del': { output: '\u2202' },
        'partial': { output: '\u2202' },
        'grad': { output: '\u2207' },
        'nabla': { output: '\u2207' },
        'and': { output: '\u2227', attributes: { form:'infix', stretchy:'true', lspace:'mediummathspace', rspace:'mediummathspace'} },
        'wedge': { output: '\u2227', attributes: { form:'infix', stretchy:'true', lspace:'mediummathspace', rspace:'mediummathspace'} },
        'And': { output: '\u22C0' },
        'bigwedge': { output: '\u22C0' },
        'or': { output: '\u2228', attributes: { form:'infix', stretchy:'true', lspace:'mediummathspace', rspace:'mediummathspace'} },
        'vee': { output: '\u2228', attributes: { form:'infix', stretchy:'true', lspace:'mediummathspace', rspace:'mediummathspace'} },
        'Or': { output: '\u22C1' },
        'bigvee': { output: '\u22C1' },
        'circ1': { output: '\u2460', attribtues: { lspace: 'thickmathspace', rspace: 'thickmathspace' } },
        'circ2': { output: '\u2461', attribtues: { lspace: 'thickmathspace', rspace: 'thickmathspace' } },
        'circ3': { output: '\u2462', attribtues: { lspace: 'thickmathspace', rspace: 'thickmathspace' } },
        'circ4': { output: '\u2463', attribtues: { lspace: 'thickmathspace', rspace: 'thickmathspace' } },
        'circ5': { output: '\u2464', attribtues: { lspace: 'thickmathspace', rspace: 'thickmathspace' } },
        'circ6': { output: '\u2465', attribtues: { lspace: 'thickmathspace', rspace: 'thickmathspace' } },
        'circ7': { output: '\u2466', attribtues: { lspace: 'thickmathspace', rspace: 'thickmathspace' } },
        'circ8': { output: '\u2467', attribtues: { lspace: 'thickmathspace', rspace: 'thickmathspace' } },
        'circ9': { output: '\u2468', attribtues: { lspace: 'thickmathspace', rspace: 'thickmathspace' } },
        'circ10': { output: '\u2469', attribtues: { lspace: 'thickmathspace', rspace: 'thickmathspace' } },
        'comma': { output: ',' },
        'com': { output: ',' },
        '\\,': { output: ',' },
        '\\;': { output: ';' },
        'nlt': { output: '\u226E' },
        'ngt': { output: '\u226F' },
        '!lt': { output: '\u226E' },
        '!gt': { output: '\u226F' },
        '!<': { output: '\u226E' },
        '!>': { output: '\u226F' },
        'plusminus': { output: '\u00B1', attributes: { lspace: '1px', rspace: '1px' } },
        'pos': { output: '+', attributes: { lspace: '1px', rspace: '1px' } },
        'lim': { output: 'lim', attributes: { movablelimits: 'false' } },
        '|~': { output: '\u2308' },
        'lceiling': { output: '\u2308' },
        '~|': { output: '\u2309' },
        'rceiling': { output: '\u2309' },
        '|__': { output: '\u230A' },
        'lfloor': { output: '\u230A' },
        '__|': { output: '\u230B' },
        'rfloor': { output: '\u230B' },
        'vvv': { output: '\u22C1' },
        'vv': { output: '\u2228', attributes: { form:'infix', stretchy:'true', lspace:'mediummathspace', rspace:'mediummathspace'} }
    };
    
    mathwalker.identifier_table = {
        // special greek
        'betaa': { output: '\u03B2', attributes: { mathvariant: 'italic' } },
        'Deltaa': { output: '\u0394', attributes: { mathvariant: 'italic' } },
        'epsia': { output: '\u03B5', attributes: { mathvariant: 'normal' } },
        'varepsilon': { output: '\u03B5', attributes: { mathvariant: 'normal' } },
        'Gammaa': { output: '\u0393', attributes: { mathvariant: 'italic' } },
        'lambdaa': { output: '\u03BB', attributes: { mathvariant: 'normal' } },
        'mua': { output: '\u03BC', attributes: { mathvariant: 'normal' } },
        'nua': { output: '\u03BD', attributes: { mathvariant: 'normal' } },
        'varphi': { output: '\u03C6', attributes: { mathvariant: 'italic' } },
        'Pia': { output: '\u03A0', attributes: { mathvariant: 'normal' } },
        'upsilona': { output: '\u03C5', attributes: { mathvariant: 'italic' } },
        // other idents
        'deg': { output: 'deg' },
        'angstrom': { output: '\u212B', attributes: { mathvariant: 'normal' } },
        'ddagger': { output: '\u2021', attributes: { mathvariant: 'normal' } },
        "''": { output: '\u2033', attributes: { mathvariant: 'normal' } },
        "'": { output: '\u2032', attributes: { mathvariant: 'normal' } },
        "'''": { output: '\u2034', attributes: { mathvariant: 'normal' } },
        "''''": { output: '\u2057', attributes: { mathvariant: 'normal' } },
        'prime': { output: '\u2032', attributes: { mathvariant: 'normal' } },
        'Prime': { output: '\u2033', attributes: { mathvariant: 'normal' } },
        'ohm': { output: '\u03A9', attributes: { mathvariant: 'normal' } },
        'cdots': { output: '\u22EF', attributes: { mathvariant: 'normal' } },
        'vdots': { output: '\u22EE', attributes: { mathvariant: 'normal' } },
        'ddots': { output: '\u22F1', attributes: { mathvariant: 'normal' } },
        '...': { output: '\u2026', attributes: { mathvariant: 'normal' } },
        'ldots': { output: '\u2026', attributes: { mathvariant: 'normal' } },
        '|~': { output: '\u2308', attributes: { mathvariant: 'normal' } },
        '~|': { output: '\u2309', attributes: { mathvariant: 'normal' } },
        'CC': { output: '\u2102', attributes: { mathvariant: 'normal' } },
        'NN': { output: '\u2115', attributes: { mathvariant: 'normal' } },
        'QQ': { output: '\u211A', attributes: { mathvariant: 'normal' } },
        'RR': { output: '\u211D', attributes: { mathvariant: 'normal' } },
        'ZZ': { output: '\u2124', attributes: { mathvariant: 'normal' } },
        'sdot': { output: '\u22C5' },
        'middot': { output: '\u00B7' },
        'permil': { output: '\u2030', attributes: { mathvariant: 'normal' } },
        '_|_': { output: '\u22A5', attributes: { mathvariant: 'normal' } },
        'bot': { output: '\u22A5', attributes: { mathvariant: 'normal' } },
        'oo': { output: '\u221E', attributes: { mathvariant: 'normal' } },
        'infty': { output: '\u221E', attributes: { mathvariant: 'normal' } },
        'O/': { output: '\u2205', attributes: { mathvariant: 'normal' } },
        'aleph': { output: '\u2135', attributes: { mathvariant: 'normal' } },
        'planck': { output: '\u210F', attributes: { mathvariant: 'normal' } },
        'pound': { output: '\u00A3', attributes: { mathvariant: 'normal' } },
        'yen': { output: '\u00A5', attributes: { mathvariant: 'normal' } },
        'euro': { output: '\u20AC', attributes: { mathvariant: 'normal' } },
        'rupee': { output: '\u20B9', attributes: { mathvariant: 'normal' } },
        'cent': { output: '\u00A2', attributes: { mathvariant: 'normal' } },
        '$': { output: '$', attributes: { mathvariant: 'normal' } },
        'currency': { output: '\u00A4', attributes: { mathvariant: 'normal' } },
        'dinar': { output: '', attributes: { mathvariant: 'normal' } },
        'rial': { output: '\u0631.\u0642', attribtues: { mathvariant: 'normal' } },
        // greek
        'alpha': { output: '\u03B1', attributes: { mathvariant: 'normal' } },
        'beta': { output: '\u03B2', attributes: { mathvariant: 'normal' } },
        'gamma': { output: '\u03B3', attributes: { mathvariant: 'italic' } },
        'delta': { output: '\u03B4', attributes: { mathvariant: 'italic' } },
        'epsi': { output: '\u03B5', attributes: { mathvariant: 'italic' } },
        'epsilon': { output: '\u03B5', attributes: { mathvariant: 'italic' } },
        'zeta': { output: '\u03B6', attributes: { mathvariant: 'normal' } },
        'eta': { output: '\u03B7', attributes: { mathvariant: 'italic' } },
        'theta': { output: '\u03B8', attributes: { mathvariant: 'italic' } },
        'iota': { output: '\u03B9', attributes: { mathvariant: 'normal' } },
        'kappa': { output: '\u03BA', attributes: { mathvariant: 'italic' } },
        'lambda': { output: '\u03BB', attributes: { mathvariant: 'italic' } },
        'mu': { output: '\u03BC', attributes: { mathvariant: 'italic' } },
        'nu': { output: '\u03BD', attributes: { mathvariant: 'italic' } },
        'xi': { output: '\u03BE', attributes: { mathvariant: 'normal' } },
        'omicron': { output: '\u03BF', attributes: { mathvariant: 'italic' } },
        'pi': { output: '\u03C0', attributes: { mathvariant: 'italic' } },
        'rho': { output: '\u03C1', attributes: { mathvariant: 'italic' } },
        'sigma': { output: '\u03C3', attributes: { mathvariant: 'italic' } },
        'tau': { output: '\u03C4', attributes: { mathvariant: 'normal' } },
        'upsilon': { output: '\u03A5', attributes: { mathvariant: 'normal' } },
        'phi': { output: '\u03D5', attributes: { mathvariant: 'normal' } },
        'chi': { output: '\u03C7', attributes: { mathvariant: 'italic' } },
        'psi': { output: '\u03C8', attributes: { mathvariant: 'italic' } },
        'omega': { output: '\u03C9', attributes: { mathvariant: 'italic' } },
        'Alpha': { output: '\u0391', attributes: { mathvariant: 'italic' } },
        'Beta': { output: '\u0392', attributes: { mathvariant: 'italic' } },
        'Gamma': { output: '\u0393', attributes: { mathvariant: 'normal' } },
        'Delta': { output: '\u0394', attributes: { mathvariant: 'normal' } },
        'Epsilon': { output: '\u0395', attributes: { mathvariant: 'italic' } },
        'Zeta': { output: '\u0396', attributes: { mathvariant: 'italic' } },
        'Eta': { output: '\u0397', attributes: { mathvariant: 'italic' } },
        'Theta': { output: '\u0398', attributes: { mathvariant: 'italic' } },
        'Iota': { output: '\u0399', attributes: { mathvariant: 'italic' } },
        'Kappa': { output: '\u039A', attributes: { mathvariant: 'italic' } },
        'Lambda': { output: '\u039B', attributes: { mathvariant: 'italic' } },
        'Mu': { output: '\u039C', attributes: { mathvariant: 'italic' } },
        'Nu': { output: '\u039D', attributes: { mathvariant: 'italic' } },
        'Xi': { output: '\u039E', attributes: { mathvariant: 'italic' } },
        'Omicron': { output: '\u039F', attributes: { mathvariant: 'italic' } },
        'Pi': { output: '\u03A0', attributes: { mathvariant: 'italic' } },
        'Rho': { output: '\u03A1', attributes: { mathvariant: 'italic' } },
        'Sigma': { output: '\u03A3', attributes: { mathvariant: 'normal' } },
        'Tau': { output: '\u03A4', attributes: { mathvariant: 'italic' } },
        'Upsilon': { output: '\u03A5', attributes: { mathvariant: 'italic' } },
        'Phi': { output: '\u03A6', attributes: { mathvariant: 'italic' } },
        'Chi': { output: '\u03A7', attributes: { mathvariant: 'italic' } },
        'Psi': { output: '\u03A8', attributes: { mathvariant: 'italic' } },
        'Omega': { output: '\u03A9', attributes: { mathvariant: 'italic' } }
    };
    mathwalker.arrow_table = {
        'rlarr': { output: '\u21C4' },
        'lrarr': { output: '\u21C6' },
        'uarr': { output: '\u2191' },
        'uArr': { output: '\u21D1' },
        'uparrow': { output: '\u21C8' },
        'darr': { output: '\u2193' },
        'dArr': { output: '\u21D3' },
        'downarrow': { output: '\u21C8' },
        'rarr': { output: '\u2192' },
        'rightarrow': { output: '\u2192' },
        'larr': { output: '\u2190' },
        'leftarrow': { output: '\u2190' },
        'harr': { output: '\u2194' },
        'varr': { output: '\u2195' },
        'leftrightarrow': { output: '\u2194' },
        'rArr': { output: '\u21D2' },
        'Rightarrow': { output: '\u21D2' },
        'lArr': { output: '\u21D0' },
        'Leftarrow': { output: '\u21D0' },
        'hArr': { output: '\u21D4' },
        'vArr': { output: '\u21D5' },
        'Leftrightarrow': { output: '\u21D4' },
        '|->': { output: '\u21A6' },
        'mapsto': { output: '\u21A6' },
        '<->': { output: '\u2194' },
        '<--': { output: '\u2190' },
        '-->': { output: '\u2192' },
        '<=>': { output: '\u21D4' },
        'iff': { output: '\u21D4' },
        '<==': { output: '\u21D0' },
        '==>': { output: '\u21D2' },
        '=>': { output: '\u21D2' },
        'implies': { output: '\u21D2' },
        '<-': { output: '\u2190' },
        '->': { output: '\u2192' },
        'to': { output: '\u2192' },
        'nwarr': { output: '\u2196' },
        'nearr': { output: '\u2197' },
        'swarr': { output: '\u2199' },
        'searr': { output: '\u2198' },
        'nwArr': { output: '\u21D6' },
        'neArr': { output: '\u21D7' },
        'seArr': { output: '\u21D8' },
        'swArr': { output: '\u21D9' },
        'rlharr': { output: '\u21CC' },
        'lrharr': { output: '\u21CB' }
    };
    mathwalker.text_table = {
        'degrees': { output: '\u00B0' },
        'ensp': { output: '\u2002' },
        'emsp': { output: '\u2003' },
        'thickspace': { output: '\u2009\u200A\u200A' },
        'emsp13': { output: '\u2004' },
        'emsp14': { output: '\u2005' },
        'emsp16': { output: '\u2006' },
        'numsp': { output: '\u2007' },
        'puncsp': { output: '\u2008' },
        'thinsp': { output: '\u2009' },
        'hairsp': { output: '\u200A' },
        'medsp': { output: '\u202F' },
        'NoBreak': { output: '\u2060' },
        'idiosp': { output: '\u3000' }
    };
    mathwalker.script_table = {
        'ul': { output: '\u00AF' },
        'underline': { output: '\u00AF' },
        'hat': { output: '\u005E' },
        'bar': { output: '\u00AF' },
        'overline': { output: '\u00AF' },
        'lvec': { output: '\u21BC' },
        'vec': { output: '\u21C0' },
        'vector': { output: '\u2192' },
        'dot': { output: '\u02D9' },
        'ddot': { output: '\u00A8' }
    };
    mathwalker.style_table = {
        'bb': { mathvariant: 'bold' },
        'mathbf': { mathvariant: 'bold' },
        'sf': { mathvariant: 'sans-serif' },
        'mathsf': { mathvariant: 'sans-serif' },
        'bbb': { mathvariant: 'double-struck' },
        'mathbb': { mathvariant: 'double-struck' },
        'cc': { mathvariant: 'script' },
        'mathcal': { mathvariant: 'script' },
        'tt': { mathvariant: 'monospace' },
        'mathtt': { mathvariant: 'monospace' },
        'fr': { mathvariant: 'fraktur' },
        'mathfrak': { mathvariant: 'fraktur' }
    };
    mathwalker.current_lookup = null;
    mathwalker.namespaceuri = "http://www.w3.org/1998/Math/MathML";
    mathwalker._applyattributes = function(attributes, element) {
        if(!attributes) return;
        
        for(var attr in attributes) {
            element.setAttribute(attr, attributes[attr]);
        }
    };
    mathwalker.style = function(treenode, parent) {
        var element = createElement('mstyle');
        var attrs = style_table[treenode.style] || {};
        _applyattributes(attrs, element);
        var prev_lookup = mathwalker.current_lookup;
        if(treenode.style == 'fr' || treenode.style == 'mathfrak') mathwalker.current_lookup = frk_lookup;
        if(treenode.style == 'cc' || treenode.style == 'mathcal') mathwalker.current_lookup = cal_lookup;
        if(treenode.style == 'bbb' || treenode.style == 'mathbb') mathwalker.current_lookup = ds_lookup;
        element.appendChild(ParseNode(treenode.atom, element));
        mathwalker.current_lookup = prev_lookup;
        return element;
    };
    mathwalker.variable = function(treenode, parent) {
        return identifier(treenode, parent);
    };
    mathwalker.diffident = function(treenode, parent) {
        var element = createElement('mi');
        element.setAttribute('mathvariant', 'italic');
        var identifier = treenode.identifier.value;
        if(identifier_table[identifier]) {
            var identinfo = identifier_table[identifier];
            if(identinfo.output !== undefined) identifier = identinfo.output;
        }
        element.appendChild(createTextNode('d' + identifier));
        return element;
    };
    mathwalker.escapedoperator = function(treenode, parent) {
        var element = createElement('mo');
        var ccode = parseInt(treenode.value, 16);
        var value = String.fromCharCode(ccode);
        element.appendChild(createTextNode(value));
        return element;
    };
    mathwalker.escapedident = function(treenode, parent) {
        var element = createElement('mi');
        var ccode = parseInt(treenode.value, 16);
        var value = String.fromCharCode(ccode);
        element.appendChild(createTextNode(value));
        return element;
    };
    mathwalker.identifier = function(treenode, parent) {
        var element = createElement('mi');
        var identifier = treenode.value;
        if(current_lookup && identifier.length == 1 && identifier.charCodeAt(0) >= 65 && identifier.charCodeAt(0) <= 90) {
            identifier = String.fromCharCode(current_lookup[identifier.charCodeAt(0)-65]);
        }
        if(identifier_table[identifier]) {
            var identinfo = identifier_table[identifier];
            identifier = identinfo.output || identifier;
            _applyattributes(identinfo.attributes, element);
        }
        element.appendChild(createTextNode(identifier));
        return element;
    };
    mathwalker.relationaloperator = function(treenode, parent) {
        return operator(treenode, parent);
    };
    mathwalker.vertical_bar = function(treenode, parent) {
        return operator(treenode, parent);
    };
    mathwalker.operator = function(treenode, parent) {
        var element = createElement('mo');
        var operator = treenode.value;
        if(operator_table[operator]) {
            var opinfo = operator_table[operator];
            operator = opinfo.output || operator;
            _applyattributes(opinfo.attributes, element);
        }
        element.appendChild(createTextNode(operator));
        return element;
    };
    mathwalker.number = function(treenode, parent) {
        var element = createElement('mn');
        element.appendChild(createTextNode(treenode.value));
        return element;
    };
    mathwalker.specialtext = function(treenode, parent) {
        var element = createElement('mtext');
        var st = treenode.value;
        if(text_table[st]) {
            var ttinfo = text_table[st];
            st = ttinfo.output || st;
            _applyattributes(ttinfo.attributes, element);
        }
        element.appendChild(createTextNode(st));
        return element;
    };
    mathwalker.normalidentifier = function(treenode, parent) {
        var element = createElement('mi');
        element.setAttribute('mathvariant', 'normal');
        element.appendChild(createTextNode(treenode.value));
        return element;
    };
    mathwalker.chemelement = function(treenode, parent) {
        return normalidentifier(treenode, parent);
    };
    mathwalker.string = function(treenode, parent) {
        var element = createElement('mtext');
        var text = treenode.value;
        text = text.replace(/ /g, '\u00A0');
        element.appendChild(createTextNode(text));
        return element;
    };
    mathwalker.align_arrow = function(treenode, parent) {
        return arrow(treenode, parent);
    };
    mathwalker.arrow = function(treenode, parent) {
        var element = createElement('mo');
        var arrow = arrow_table[treenode.value];
        _applyattributes(arrow.attributes, element);
        element.appendChild(createTextNode(arrow.output));
        return element;
    };
    
    mathwalker.func = function(treenode, parent) {
        var element = createElement('mrow');
        
        var namemi = createElement('mi');
        var first;
        namemi.appendChild(createTextNode(treenode.name));
        if(treenode.sup && treenode.sub) {
            first = createElement('msubsup');
            first.appendChild(namemi);
            first.appendChild(ParseNode(treenode.sub, first));
            first.appendChild(ParseNode(treenode.sup, first));
        }
        else if(treenode.sup && !treenode.sub) {
            first = createElement('msup');
            first.appendChild(namemi);
            first.appendChild(ParseNode(treenode.sup, first));
        }
        else if(!treenode.sup && treenode.sub) {
            first = createElement('msub');
            first.appendChild(namemi);
            first.appendChild(ParseNode(treenode.sub, first));
        }
        else {
            first = namemi;
        }
        element.appendChild(first);
        
        if(treenode.primes) {
            element.appendChild(ParseNode(treenode.primes, element));
        }
        
        var applyFunctionMO = createElement('mo');
        applyFunctionMO.appendChild(createTextNode('\u2061'));
        element.appendChild(applyFunctionMO);
        
        element.appendChild(ParseNode(treenode.of, element));
        return element;
    };
    
    mathwalker.enclosure = function(treenode, parent) {
      var element = createElement('menclose');
      element.setAttribute('notation', treenode.notation);
      ParseChildren(treenode.children, element);
      return element;
    };
    mathwalker.phantom = function(treenode, parent) {
        var element = createElement('mphantom');
        ParseChildren(treenode.children, element);
        return element;
    };
    
    mathwalker.root = function(treenode, parent) {
        var element = createElement('mroot');
        element.appendChild(ParseNode(treenode.base, element));
        element.appendChild(ParseNode(treenode.nth, element));
        return element;
    };
    mathwalker.sqrt = function(treenode, parent) {
        var element = createElement('msqrt');
        element.appendChild(ParseNode(treenode.base, element));
        return element;
    };
    
    mathwalker.tablecell = function(treenode, parent, align) {
        var element = createElement('mtd');
        if(align !== undefined) {
            element.setAttribute('columnalign', align);
        }
        ParseChildren(treenode.children, element);
        return element;
    };
    mathwalker.tablerow = function(treenode, parent, opts) {
        if(opts === undefined) opts = {};
        var element = createElement('mtr');
        ParseChildren(treenode.cells, element, opts.aligns);
        if(opts.columnCount !== undefined && opts.columnCount > treenode.cells.length) {
            var extraCount = opts.columnCount - treenode.cells.length;
            for(var i = 0; i < extraCount; i++) {
                element.appendChild(createElement('mtd'));
            }
        }
        return element;
    };
    mathwalker.table = function(treenode, parent) {
        var element = createElement('mtable');
        
        var columnCount = 0;
        for(var i = 0; i < treenode.rows.length; i++) {
            if(treenode.rows[i].row && treenode.rows[i].row.cells.length > columnCount) columnCount = treenode.rows[i].row.cells.length;
        }
        
        var aligns = [];
        if(treenode.options && treenode.options.aligns.length > 0) {
            aligns = treenode.options.aligns;
            while(aligns.length < columnCount) {
                aligns.push(aligns[aligns.length-1]);
            }

            element.setAttribute('columnalign', aligns.join(" "));
        }
        var rargs = { 'aligns': aligns, 'columnCount': columnCount };
        
        var args = [];
        for(var i = 0; i < treenode.rows.length; i++) args.push(rargs);
        
        var rows = [];
        var rlines = [];
        var haslines = false;
        
        for(var i = 0; i < treenode.rows.length; i++) {
            rows.push(treenode.rows[i].row);
            if(i >= 1) {
                if(treenode.rows[i].line) {
                    rlines.push('solid');
                    haslines = true;
                }
                else {
                    rlines.push('none');
                }
            }
        }
        
        ParseChildren(rows, element, args);
        if(treenode.options) {
            var opts = treenode.options;
            if(opts.column) {
                element.setAttribute('columnspacing', opts.column.size + opts.column.unit);
            }
            if(opts.row) {
                element.setAttribute('rowspacing', opts.row.size + opts.row.unit);
            }
            if(opts.horizontalframe) {
                var fs = opts.horizontalframe.size + opts.horizontalframe.unit;
                if(opts.verticalframe) {
                    fs += ' ' + opts.verticalframe.size + opts.verticalframe.unit;
                }
                element.setAttribute('framespacing', fs);
            }
        }
        if(treenode.nolines) {
            if(haslines) {
                element.setAttribute('rowlines', rlines.join(" "));
            }
        }
        else {
            element.setAttribute('rowlines', 'solid');
            element.setAttribute('columnlines', 'solid');
            element.setAttribute('frame', 'solid');
        }
        return element;
    };
    mathwalker.matrix = function(treenode, parent) {
        var element = createElement('mfenced');
        element.setAttribute('separators', '');
        var left = treenode.left || '';
        var opinfo = operator_table[left];
        if(opinfo && opinfo.output !== undefined) left = opinfo.output;
        element.setAttribute('open', left);
        var right = treenode.right || '';
        opinfo = operator_table[right];
        if(opinfo && opinfo.output !== undefined) right = opinfo.output;
        element.setAttribute('close', right);
        var table = createElement('mtable');
        var columnCount = 0;
        for(var i = 0; i < treenode.rows.length; i++) {
            if(treenode.rows[i].row && treenode.rows[i].row.cells.length > columnCount) columnCount = treenode.rows[i].row.cells.length;
        }
        
        if(treenode.split) {
            var pos = Math.floor(columnCount/2) - 1;
            var clines = [];
            for(var i = 0; i < columnCount - 1; i++) {
                if(i == pos) {
                    clines.push('solid');
                }
                else {
                    clines.push('none');
                }
            }
            
            table.setAttribute('columnlines', clines.join(" "));
        }
        
        var rows = [];
        var rlines = [];
        var haslines = false;
        
        for(var i = 0; i < treenode.rows.length; i++) {
            rows.push(treenode.rows[i].row);
            if(i >= 1) {
                if(treenode.rows[i].line) {
                    rlines.push('solid');
                    haslines = true;
                }
                else {
                    rlines.push('none');
                }
            }
        }
        
        if(haslines) {
            element.setAttribute('rowlines', rlines.join(" "));
        }
        
        for(var i = 0; i < treenode.rows.length; i++) {
            var celement = ParseNode(treenode.rows[i].row, table, { columnCount: columnCount });
            table.appendChild(celement);
        }
        element.appendChild(table);
        return element;
    };
    mathwalker.alignrow_terms_split = function(terms) {
        var left = [];
        var op = null;
        var right = [];
        for(var i = 0; i < terms.length; i++) {
            var t = terms[i];
            if((t.type == 'relationaloperator' || t.type=='align_arrow') && op === null) {
                op = t;
            }
            else if(op === null) {
                left.push(t);
            }
            else {
                right.push(t);
            }
        }
        
        if(op === null) {
            right = left;
            left = null;
        }
        
        return { 'left': left, 'right': right, 'operator': op };
    };
    mathwalker.alignrow = function(treenode, parent) {
        var row = createElement('mtr');
        var ar = alignrow_terms_split(treenode.terms);
        if(ar.operator) { mathwalker.lastrelational = ar.operator; }
        
        var leftcell = createElement('mtd');
        leftcell.setAttribute('columnalign', 'right');
        if(ar.left) {
            ParseChildren(ar.left, leftcell);
        }
        row.appendChild(leftcell);
        
        var rightcell = createElement('mtd');
        rightcell.setAttribute('columnalign', 'left');
        var opnode = ParseNode(mathwalker.lastrelational, rightcell);
        if(opnode) {
            rightcell.appendChild(opnode);
        }
        
        if(ar.right) {
            ParseChildren(ar.right, rightcell);
        }
        row.appendChild(rightcell);
        return row;
    };
    mathwalker.align = function(treenode, parent) {
        var element = createElement('mtable');
        element.setAttribute('columnspacing', '0.1em');
        element.setAttribute('columnalign', 'right left');
        mathwalker.lastrelational = null;
        
        var rows = [];
        var rlines = [];
        var haslines = false;
        
        for(var i = 0; i < treenode.rows.length; i++) {
            rows.push(treenode.rows[i].row);
            if(i >= 1) {
                if(treenode.rows[i].line) {
                    rlines.push('solid');
                    haslines = true;
                }
                else {
                    rlines.push('none');
                }
            }
        }
        
        if(haslines) {
            element.setAttribute('rowlines', rlines.join(" "));
        }
        
        for(var i = 0; i < rows.length; i++) {
            element.appendChild(ParseNode(rows[i], element));
        }
        return element;
    };
    
    mathwalker.subsupscript = function(treenode, parent) {
        var element = createElement('msubsup');
        element.appendChild(ParseNode(treenode.base, element));
        element.appendChild(ParseNode(treenode.sub, element));
        element.appendChild(ParseNode(treenode.sup, element));
        return element;
    };
    mathwalker.superscript = function(treenode, parent) {
        var element = createElement('msup');
        element.appendChild(ParseNode(treenode.base, element));
        element.appendChild(ParseNode(treenode.script, element));
        return element;
    };
    mathwalker.subscript = function(treenode, parent) {
        var element = createElement('msub');
        element.appendChild(ParseNode(treenode.base, element));
        element.appendChild(ParseNode(treenode.script, element));
        return element;
    };
    mathwalker.multiscripts = function(treenode, parent) {
        var element = createElement('mmultiscripts');
        element.appendChild(ParseNode(treenode.base, element));
        if(treenode.post) {
            element.appendChild(ParseNode(treenode.post.sub, element));
            element.appendChild(ParseNode(treenode.post.sup, element));
        }
        if(treenode.pre) {
            element.appendChild(createElement('mprescripts'));
            element.appendChild(ParseNode(treenode.pre.sub, element));
            element.appendChild(ParseNode(treenode.pre.sup, element));
        }
        return element;
    };
    mathwalker.specialover = function(treenode, parent) {
        var element = createElement('mover');
        element.appendChild(ParseNode(treenode.base, element));
        var info = script_table[treenode.script] || {};
        var mo = createElement('mo');
        mo.appendChild(createTextNode(info.output || ''));
        element.appendChild(mo);
        return element;
    };
    mathwalker.over = function(treenode, parent) {
        var element = createElement('mover');
        var basenode = ParseNode(treenode.base, element);
        if (treenode.movablelimits === false) {
                basenode.setAttribute('movablelimits', 'false');
        }
        element.appendChild(basenode);
        element.appendChild(ParseNode(treenode.script, element));
        return element;
    };
    mathwalker.specialunder = function(treenode, parent) {
        var element = createElement('munder');
        element.appendChild(ParseNode(treenode.base, element));
        var info = script_table[treenode.script] || {};
        var mo = createElement('mo');
        mo.appendChild(createTextNode(info.output || ''));
        element.appendChild(mo);
        return element;
    };
    mathwalker.under = function(treenode, parent) {
        var element = createElement('munder');
        var basenode = ParseNode(treenode.base, element);
        if (treenode.movablelimits === false) {
                basenode.setAttribute('movablelimits', 'false');
        }
        element.appendChild(basenode);
        element.appendChild(ParseNode(treenode.script, element));
        return element;
    };
    mathwalker.overunder = function(treenode, parent) {
        var element = createElement('munderover');
        var basenode = ParseNode(treenode.base, element);
        if (treenode.movablelimits === false) {
                basenode.setAttribute('movablelimits', 'false');
        }
        element.appendChild(basenode);
        element.appendChild(ParseNode(treenode.under, element));
        element.appendChild(ParseNode(treenode.over, element));
        return element;
    };
    mathwalker.fraction = function(treenode, parent) {
        var element = createElement('mfrac');
        element.setAttribute('bevelled', treenode.bevelled);
        element.appendChild(ParseNode(treenode.numerator, element));
        element.appendChild(ParseNode(treenode.denominator, element));
        return element;
    };
    mathwalker._use_row_always = ['mfrac', 'mroot', 'msqrt', 'mover', 'munder', 'munderover', 'mstyle'];
    mathwalker._use_row_plusone = ['msub', 'msup', 'mmultiscripts', 'msubsup'];
    mathwalker.fence = function(treenode, parent) {
        var element;
        if(treenode.left == '(' && (treenode.right == ')' || treenode.right === '')) {
            var ln = parent.localName;
            if(indexOf(_use_row_always, ln) >= 0 || (indexOf(_use_row_plusone, ln) >= 0 && parent.childNodes.length >= 1)) {
                element = createElement('mrow');
            }
            else {
                element = createElement('mfenced');
                element.setAttribute('separators', '');
                element.setAttribute('open', treenode.left);
                element.setAttribute('close', treenode.right);
            }
        }
        else {
            element = createElement('mfenced');
            element.setAttribute('separators', '');
            var open = treenode.left;
            if(operator_table[open]) { open = operator_table[open].output; }
            var close = treenode.right;
            if(operator_table[close]) { close = operator_table[close].output; }
            element.setAttribute('open', open);
            element.setAttribute('close', close);
        }
        
        ParseChildren(treenode.children, element);
        return element;
    };
    
    mathwalker.longdiv = function(treenode, parent) {
        var element = createElement('mtable');
        element.setAttribute('rowspacing', '5px');
        element.setAttribute('columnspacing', '0px');
        element.setAttribute('columnalign', 'right left');
        var hasremain = false;
        if(treenode.result) {
            var mtr = createElement('mtr');
            element.appendChild(mtr);
            mtr.appendChild(ParseNode(treenode.result, mtr, 'right'));
            if(treenode.remainder.children.length > 0) {
                var mtd = ParseNode(treenode.remainder, mtr, 'left');
                mtr.appendChild(mtd);
                var r = createElement('mtext');
                r.appendChild(createTextNode('\u00A0R\u00A0'));
                mtd.insertBefore(r, mtd.firstChild);
                hasremain = true;
            }
        }
        var mtr = createElement('mtr');
        element.appendChild(mtr);
        var mtd = createElement('mtd');
        mtr.appendChild(mtd);
        mtd.setAttribute('columnalign', 'right');
        ParseChildren(treenode.divisor.children, mtd);
        
        var ldenclose = createElement('menclose');
        ldenclose.setAttribute('notation', 'longdiv');
        mtd.appendChild(ldenclose);
        ParseChildren(treenode.dividend.children, ldenclose);
        
        if(hasremain) {
            var t = createElement('mtd');
            mtr.appendChild(t);
        }
        
        for(var i = 0; i < treenode.steps.length; i++) {
            var step = treenode.steps[i];
            var mtr = createElement('mtr');
            element.appendChild(mtr);
            mtr.appendChild(ParseNode(step, mtr, 'right'));
            if(hasremain) {
                var t = createElement('mtd');
                mtr.appendChild(t);
            }
        }
        
        return element;
    };
    
    mathwalker.autolongdiv = function(treenode, parent) {
        var divisor = parseInt(treenode.divisor.value || "1", 10);
        var dividend = parseInt(treenode.dividend.value || "0", 10);
        var step = parseInt(treenode.step || "-1", 10);
        var solution = LongDivisionSolver(divisor, dividend, step, false);
        treenode.result = solution.result;
        treenode.steps = solution.steps;
        treenode.remainder = solution.remainder;
        treenode.divisor = { 'type': 'tablecell', 'children': [treenode.divisor] };
        treenode.dividend = { 'type': 'tablecell', 'children': [treenode.dividend] };
        return longdiv(treenode, parent);
    }
    
    mathwalker.size = function(treenode) {
        var normalize_unit = function(unit) {
            if(!unit || unit === "") { return "em"; }
            if(unit == "e") { return "em"; }
            if(unit == "p") { return "px"; }
            return unit;
        };
        
        return treenode.size + normalize_unit(treenode.unit);
    };
    mathwalker.ParseChildren = function(children, parent, args) {
        if(args === undefined) { args = []; }
        var has_vertical_bar = false;
        for(var i = 0; i < children.length; i++) {
            if(children[i].type == 'vertical_bar') has_vertical_bar = true;
        }
        if(children.length > 2 && children[0].type == 'vertical_bar' && children[children.length-1].type == 'vertical_bar') {
            var fenced = createElement('mfenced');
            fenced.setAttribute('separators', '');
            var open = children[0].value;
            if(operator_table[open]) { open = operator_table[open].output; }
            var close = children[children.length-1].value;
            if(operator_table[close]) { close = operator_table[close].output; }
            fenced.setAttribute('open', open);
            fenced.setAttribute('close', close);
            has_vertical_bar = false;
            children = children.slice(1, children.length - 1);
            parent.appendChild(fenced);
            parent = fenced;
        }
        if(has_vertical_bar && parent.localName != 'mrow' && parent.localName != 'mfenced') {
            var row = createElement('mrow');
            parent.appendChild(row);
            parent = row;
        }
        for(var i = 0; i < children.length; i++) {
            parent.appendChild(ParseNode(children[i], parent, args[i]));
        }
    };
    mathwalker.ParseNode = function(treenode, parent, args) {
        if(!treenode) {
            return createElement('mi');
        }
        var func = mathwalker[treenode.type];
        if(!func) {
            return createElement('mi');
        }
        var element = func(treenode, parent, args);
        if(element) {
            return element;
        }
        else {
            return createElement('mi');
        }
    };
    mathwalker.Walk = function(treenode) {
        createDocument();
        var e = doc.documentElement;
        ParseChildren(treenode.children, e);
        return doc;
    };
    mathwalker.createDocument = function() {
        if(document.implementation.createDocument) {
            mathwalker.doc = document.implementation.createDocument('http://www.w3.org/1998/Math/MathML', 'math', null);
            mathwalker.doc.documentElement.setAttribute('display', 'block');
        }
        else {
            mathwalker.doc = new ActiveXObject('Msxml2.DOMDocument.3.0');
            var root = doc.createNode(1, 'math', namespaceuri);
            root.setAttribute('display', 'block');
            doc.appendChild(root);
        }
    };
    mathwalker.createElement = function(tag) {
        if(doc.createElementNS) {
            return doc.createElementNS(namespaceuri, tag);
        }
        else {
            return doc.createNode(1, tag, namespaceuri);
        }
    };
    mathwalker.createTextNode = function(text) {
        return doc.createTextNode(text);
    };
    mathwalker.indexOf = function(array, item) {
        if(array.indexOf) return array.indexOf(item);
        for(var i = 0; i < array.length; i++) {
            if(array[i] === item) return i;
        }
        return -1;
    };
}


function CreateMathMLNodes(input) {
    return mathwalker.Walk(mathparser.parse(input));
}

function LongDivisionSolver(divisor, dividend, step) {
    if(step < 0) step = undefined;
    var skipzero = true;
    var divString = dividend.toString();
    var resultString = '';
    var working = '';
    var remain;
    var steps = [];
    var start = divisor.toString().length - 1;
    var resultpad = '';
    for(var i = 0; i < divString.length; i++) {
        if(((i-start)*2) == step) {
            resultpad = divString.substring(i);
            remain = 0;
            break;
        }
        working = working + divString[i];
        var workingnum = parseInt(working, 10);
        var res = Math.floor(workingnum/divisor);
        remain = workingnum % divisor;
        working = remain.toString();
        if(resultString !== '' || res !== 0) resultString += res;
        var padCount = divString.length - i - 2;
        var pad = '';
        if(padCount > 0) pad = divString.substring(i+2);
            
        var row1 = { 'type': 'tablecell', 'children': [] };
        row1.children.push({ 'type': 'operator', 'value': '-' });
        row1.children.push({ type: 'specialunder', 'base': { 'type': 'number', 'value': "" + (res*divisor) }, 'script': 'ul' });
        
        if(((i-start)*2 + 1) == step) {
            pad = divString.substring(i+1);
            if(i < divString.length - 1) {
                row1.children.push({ 'type': 'phantom', 'children': [{ 'type': 'number', 'value': pad }] });
            }
        }
        else {
            if(i < divString.length - 1) {
                row1.children.push({ 'type': 'escapedident', 'value': '2193' });
            }
            if(i < divString.length - 2) {
                row1.children.push({ 'type': 'phantom', 'children': [{ 'type': 'number', 'value': pad }] });
            }
        }
        
        if(i >= start && (!skipzero || res !== 0)) steps.push(row1);
        
        if(((i-start)*2 + 1) == step) {
            resultpad = divString.substring(i+1);
            remain = 0;
            break;
        }
        
        
        var row2 = { 'type': 'tablecell', 'children': [] };
        if(i < divString.length - 1) {
            var tmp = "";
            if(remain !== 0) tmp += remain;
            tmp += divString[i+1];
            
            row2.children.push({ 'type': 'number', 'value': tmp });
            if(i < divString.length - 2) {
                row2.children.push({ 'type': 'phantom', 'children': [{ 'type': 'number', 'value': pad }] });
            }
        }
        else {
            row2.children.push({ 'type': 'number', 'value': ""+remain });
        }
        
        if(i >= start && (!skipzero || res !== 0)) steps.push(row2);
        
        if(res !== 0) skipzero = false;
        
    }
    var resultnode = { 'type': 'tablecell', 'children': [] };
    resultnode.children.push({ 'type': 'number', 'value': resultString });
    if(resultpad) {
        resultnode.children.push({ 'type': 'phantom', 'children': [{ 'type': 'number', 'value': resultpad }] });
    }
    var remaindernode = { 'type': 'tablecell', 'children': [] };
    if(remain !== 0) {
        remaindernode.children.push({ 'type': 'number', 'value': "" + remain });
    }
    return { 'result': resultnode, 'remainder': remaindernode, 'steps': steps };
}