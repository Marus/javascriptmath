import sys

f = open(sys.argv[1], 'rb')
data = f.read()
u = unicode(data, 'utf-8')

output = u''

for c in u:
    if ord(c) > 127:
        output += '\\u%04X' % (ord(c), )
    else:
        output += c

print output 